﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppRuleDetailService
    {
        public bool Save(AppRuleDetail ruleDetatil)
        {
            using (var uow = new UnitOfWork())
            {
                var manager = new AppRuleDetailManager(uow);
                if (ruleDetatil.AppRuleDetailId == 0)
                {
                    manager.Add(ruleDetatil);
                }
                else
                {
                    var dbRuleDetatil = manager.GetById(ruleDetatil.AppRuleDetailId);

                    dbRuleDetatil.AppRuleId = ruleDetatil.AppRuleId;
                    dbRuleDetatil.ValidationTypeId = ruleDetatil.ValidationTypeId;
                    dbRuleDetatil.ConditionWithPreviousRuleStep = ruleDetatil.ConditionWithPreviousRuleStep;
                    dbRuleDetatil.OrderId = ruleDetatil.OrderId;
                    dbRuleDetatil.ComparisonOperatorTypeId = ruleDetatil.ComparisonOperatorTypeId;
                    dbRuleDetatil.ComparisonValue = ruleDetatil.ComparisonValue;
                    dbRuleDetatil.ComparisonValueDataTypeId = ruleDetatil.ComparisonValueDataTypeId;
                    dbRuleDetatil.ComparisonColumnId = ruleDetatil.ComparisonColumnId;
                    dbRuleDetatil.ComparisonRowId = ruleDetatil.ComparisonRowId;
                    dbRuleDetatil.ComparisonPercent = ruleDetatil.ComparisonPercent;
                    dbRuleDetatil.YearId = ruleDetatil.YearId;
                    dbRuleDetatil.ClientId = ruleDetatil.ClientId;
                    dbRuleDetatil.ComparisonDatasetId = ruleDetatil.ComparisonDatasetId;
                    dbRuleDetatil.CustomScript = ruleDetatil.CustomScript;
                    dbRuleDetatil.ValidationMessage = ruleDetatil.ValidationMessage;
                }

                return uow.SaveChanges();
            }
        }
    }
}
