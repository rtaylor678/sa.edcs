﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppRowService
    {
        public bool Update(AppRow row)
        {
            using (var uow = new UnitOfWork())
            {
                var dbRow = new AppRowManager(uow).GetById(row.AppRowId);

                dbRow.ClientId = row.ClientId;
                dbRow.RowOrder = row.RowOrder;
                dbRow.AssociateColumnId = row.AssociateColumnId;
                dbRow.Name = row.Name;
                dbRow.Year = row.Year;
                dbRow.ClientId = row.ClientId;
                dbRow.IsGroupRow = row.IsGroupRow;
                dbRow.IsReadOnly = row.IsReadOnly;
                dbRow.Status = row.Status;

                return uow.SaveChanges();
            }
        }
    }
}
