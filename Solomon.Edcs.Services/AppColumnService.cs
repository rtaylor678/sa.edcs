﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppColumnService
    {
        public IList<AppColumn> GetColumnList(int tableId, int? year = 0, int? clientId = 0)
        {
            using (var uow = new UnitOfWork())
            {
                return new AppColumnManager(uow).GetAll(x => x.AppTableId == tableId && (x.Year == null || x.Year == year) && (x.ClientId == null || x.ClientId == clientId)).ToList();
            }
        }

        public bool Update(AppColumn column)
        {
            using (var uow = new UnitOfWork())
            {
                var dbColumn = new AppColumnManager(uow).GetById(column.AppColumnId);

                dbColumn.ClientId = column.ClientId;
                dbColumn.ColumnOrder = column.ColumnOrder;
                dbColumn.DataTypeId = column.DataTypeId;
                dbColumn.DisplayName = column.DisplayName;
                dbColumn.Year = column.Year;
           
                return uow.SaveChanges();
            }
        }
    }
}
