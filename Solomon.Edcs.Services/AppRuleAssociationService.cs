﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppRuleAssociationService
    {
        public bool Save(List<AppRuleAssociation> appruleassociations)
        {
            var manager = new AppRuleAssociationManager(Uow);

            foreach (var appruleassociation in appruleassociations)
            {
                manager.Add(appruleassociation);
            }

            return Uow.SaveChanges();
        }
    }
}
