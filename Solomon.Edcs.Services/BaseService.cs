﻿
using Solomon.Edcs.Repository;
namespace Solomon.Edcs.Services
{
    public class BaseService
    {
        IUnitOfWork _uow;
        public BaseService()
        {
            _uow = new UnitOfWork();
        }
        public BaseService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        
        public IUnitOfWork Uow
        {
            get { return _uow; }            
        }
    }
}
