﻿using Solomon.Edcs.Components;
using Solomon.Edcs.Model;
using Solomon.Edcs.Repository;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Services
{
    public partial class AppTableService
    {
        public IList<AppTable> GetTableList(int study, int? year = 0, int? clientId = 0)
        {
            using (var uow = new UnitOfWork())
            {
                return new AppTableManager(uow).GetTableList(study, year, clientId);
            }
        }

        public IList<GetTableDataCellValue_Result> GetTableData(int tableId, int? year = 0, int? clientId = 0, int dataSetId = 0)
        {
            using (var uow = new UnitOfWork())
            {
                return new AppTableManager(uow).GetTableData(tableId, year, clientId, dataSetId);
            }
        }

        public IList<dynamic> GetDynamicTableData(int tableId, int? year = 0, int? clientId = 0, int dataSetId = 0)
        {
            using (var uow = new UnitOfWork())
            {
                return new AppTableManager(uow).GetDynamicTableData(tableId, year, clientId, dataSetId);
            }
        }

        public bool UpdateTable(AppTable table)
        {
            using (var uow = new UnitOfWork())
            {
                var dbTable = new AppTableManager(uow).GetById(table.AppTableId);
                dbTable.Client = table.Client;
                dbTable.DisplayName = table.DisplayName;
                dbTable.Name = table.Name;
                dbTable.Status = table.Status;
                dbTable.Study = table.Study;
                dbTable.Year = table.Year;
                
                return uow.SaveChanges();
            }
        }

        public bool SaveCellValue(int dataSetId, List<TableCellValue> data)
        {
            return AppTableManager.SaveTableData(dataSetId, data);
        }
    }
}
