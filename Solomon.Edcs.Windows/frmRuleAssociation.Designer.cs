﻿namespace Solomon.Edcs.Windows
{
    partial class frmRuleAssociation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlRules = new System.Windows.Forms.ComboBox();
            this.dgRuleAssociations = new System.Windows.Forms.DataGridView();
            this.btnDeleteAssociation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgRuleAssociations)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlRules
            // 
            this.ddlRules.FormattingEnabled = true;
            this.ddlRules.Location = new System.Drawing.Point(24, 28);
            this.ddlRules.Name = "ddlRules";
            this.ddlRules.Size = new System.Drawing.Size(556, 21);
            this.ddlRules.TabIndex = 0;
            this.ddlRules.SelectionChangeCommitted += new System.EventHandler(this.ddlRules_SelectionChangeCommitted);
            // 
            // dgRuleAssociations
            // 
            this.dgRuleAssociations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRuleAssociations.Location = new System.Drawing.Point(24, 71);
            this.dgRuleAssociations.Name = "dgRuleAssociations";
            this.dgRuleAssociations.Size = new System.Drawing.Size(631, 361);
            this.dgRuleAssociations.TabIndex = 1;
            // 
            // btnDeleteAssociation
            // 
            this.btnDeleteAssociation.Location = new System.Drawing.Point(24, 450);
            this.btnDeleteAssociation.Name = "btnDeleteAssociation";
            this.btnDeleteAssociation.Size = new System.Drawing.Size(138, 23);
            this.btnDeleteAssociation.TabIndex = 2;
            this.btnDeleteAssociation.Text = "Delete Association";
            this.btnDeleteAssociation.UseVisualStyleBackColor = true;
            this.btnDeleteAssociation.Click += new System.EventHandler(this.btnDeleteAssociation_Click);
            // 
            // frmRuleAssociation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 518);
            this.Controls.Add(this.btnDeleteAssociation);
            this.Controls.Add(this.dgRuleAssociations);
            this.Controls.Add(this.ddlRules);
            this.Name = "frmRuleAssociation";
            this.Text = "frmRuleAssociation";
            this.Load += new System.EventHandler(this.frmRuleAssociation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRuleAssociations)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlRules;
        private System.Windows.Forms.DataGridView dgRuleAssociations;
        private System.Windows.Forms.Button btnDeleteAssociation;
    }
}