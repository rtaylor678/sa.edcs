﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Solomon.Edcs.Windows
{
    public partial class frmDesignTable : Form
    {
        public int selectedTable = 0;
        public string study = "";
        public bool IsDesignRows = false;
        
        public frmDesignTable()
        {
            InitializeComponent();
            DataEvent.DataChanged += DataEvent_DataChanged;
        }

        private void frmEditTable_Load(object sender, EventArgs e)
        {
            var table = new AppTableService().GetById(selectedTable);
            txtTableName.Text = table.Name;
            txtTableDisplayName.Text = table.DisplayName;
            txtStudy.Text = study;
            if (IsDesignRows)
            {
                btnAddNewColumn.Text = "Add New Row";
                btnDeleteColumn.Text = "Delete Selected Row";
                btnUpdateColumn.Text = "Update Selected Row";
            }

            BindColumnGrid();
        }

        private void BindColumnGrid()
        {
            if (IsDesignRows)
            {
                var rows = new AppRowService().GetAll(x => x.AppTableId == selectedTable).ToList();
                dgColumn.DataSource = rows;
                dgColumn.Columns[0].Visible = false;
                dgColumn.Columns[2].Visible = false;
            }
            else
            {
                var columns = new AppColumnService().GetAll(x => x.AppTableId == selectedTable).ToList();
                dgColumn.DataSource = columns;
                dgColumn.Columns[0].Visible = false;
                dgColumn.Columns[3].Visible = false;
                dgColumn.Columns[5].Visible = false;

                var lookupService = new AppLookupService();

                dgColumn.Columns.Add(new DataGridViewComboBoxColumn
                {
                    Name = "DataType",
                    DataSource = lookupService.GetAll(x => x.AppLookupType.Name == "AppDataType").ToList(),
                    HeaderText = "Data Type",
                    ValueType = typeof(int),
                    DataPropertyName = "DataTypeId",
                    DisplayMember = "Name",
                    ValueMember = "Value",
                });
            }

            

            //var data = lookupService.GetAll(x => x.LookupType.Name == "StudyYear").ToList();
            //data.Add(new Lookup());
            //dgColumn.Columns.Add(new DataGridViewComboBoxColumn
            //{
            //    Name = "DataType",
            //    DataSource = data,
            //    HeaderText = "Data Type",
            //    ValueType = typeof(int),
            //    DataPropertyName = "DataTypeId",
            //    DisplayMember = "Name",
            //    ValueMember = "Value",
            //});
            //data = lookupService.GetAll(x => x.LookupType.Name == "Client").ToList();
            //data.Add(new Lookup());
            //dgColumn.Columns.Add(new DataGridViewComboBoxColumn
            //{
            //    Name = "DataType",
            //    DataSource = data,
            //    HeaderText = "Data Type",
            //    ValueType = typeof(int),
            //    DataPropertyName = "DataTypeId",
            //    DisplayMember = "Name",
            //    ValueMember = "Value",
            //});

            
            ////Save/Delete column 
            //DataGridViewImageColumn saveDeleteColumn = new DataGridViewImageColumn();
            //saveDeleteColumn.Image = imgList.Images["DeleteRed.png"];
            //saveDeleteColumn.Width = 20;
            //saveDeleteColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //dgColumn.Columns.Add(saveDeleteColumn);
        }

        private void btnUpdateColumn_Click(object sender, EventArgs e)
        {
            var frm = new frmAddColumn();
            frm.tableId = selectedTable;
            var id = dgColumn.SelectedRows[0].Cells[0].Value.ToString();
            int columnId = 0;
            int.TryParse(id, out columnId);
            frm.selectedColumnId = columnId;
            frm.IsRowAction = IsDesignRows;
            frm.newColumn = false;
            frm.Show();
        }

        private void btnAddNewColumn_Click(object sender, EventArgs e)
        {
            var frm = new frmAddColumn();
            frm.tableId = selectedTable;
            frm.IsRowAction = IsDesignRows;
            frm.newColumn = true;
            frm.Show();
        }

        void DataEvent_DataChanged(object sender, System.EventArgs e)
        {
            dgColumn.Refresh();

            if (dgColumn.Columns.Contains("DataType"))
            {
                dgColumn.Columns.Remove("DataType");
            }

            BindColumnGrid();
            this.Refresh();
        }

        private void btnDeleteColumn_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Delete Column", "Delete Column", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                var row = dgColumn.CurrentRow;
                int columnId = 0;
                int.TryParse(Convert.IsDBNull(row.Cells[0].Value) ? "0" : row.Cells[0].Value.ToString(), out columnId);

                if (columnId > 0)
                {
                    if (new AppColumnService().Delete(new AppColumn { AppColumnId = columnId }))
                    {
                        MessageBox.Show("Column Deleted successfully.", "Delete Column");
                        BindColumnGrid();
                        this.Refresh();
                    }
                    else
                    {
                        MessageBox.Show("Column was not Deleted. Please make sure there is no reference data for this column", "Delete Column");
                    }
                }
            }
        }
    }    
}
