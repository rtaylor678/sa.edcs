﻿using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.Edcs.Utility;
using Solomon.Edcs.Model;

namespace Solomon.Edcs.Windows
{
    public partial class frmAddEditRuleDetails : Form
    {
        public int selectedRuleDetailId = 0;
        public int selectedRuleId = 0;
        public int selectedStudyId = 0;
        
        public frmAddEditRuleDetails()
        {
            InitializeComponent();
            BindDataSource();
        }

        private void BindDataSource()
        {
            
            var service = new AppLookupService();
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "ComparisonOperator").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlComparisonOperatorType);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "AppDataType").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlComparisonValueDataType);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "AppValidationType").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlValidationType);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "BasicBoolOperator").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlConditionWithPrevReulStep);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "StudyYear").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlYear);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Client").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlClient);
            PopulateTable();
        }

        private void ddlValidationType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ShowCompareValidatorItems(false, false);
            ShowCustomValidationItems(false);
            
            if (((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key == 2 || ((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key == 5)
            {
                ShowCompareValidatorItems(true, true);
            }
            
            if (((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key == 4)
            {
                ShowCustomValidationItems(true);
            }
        }

        private void frmAddEditRuleDetails_Load(object sender, EventArgs e)
        {
            PopulateForm();            
        }

        private void PopulateTable()
        {
            var service = new AppTableService();

            Utility.BindComboBox(service.GetTableList(Global.SelectedStudyId, ddlYear.SelectedItem != null ? ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key : -1, ddlClient.SelectedItem != null ? ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key : -1)
                .Select(x => new KeyValuePair<int, string>(x.AppTableId, x.Name))
                .ToList(), ddlTable);
        }

        private void PopulateForm()
        {
            ShowCompareValidatorItems(false, false);
            ShowCustomValidationItems(false);
            
            txtStudy.Text = Global.SelectedStudy;

            var rule = new AppRuleService().GetById(selectedRuleId);
            if (rule != null)
            {
                txtRuleName.Text = rule.Name;
                txtRuleDisplayName.Text = rule.DisplayName;
            }
            var ruleDetail = new AppRuleDetailService().GetById(selectedRuleDetailId);
            if (ruleDetail != null)
            {
                ddlValidationType.SelectedValue = ruleDetail.ValidationTypeId;
                
                if (ruleDetail.ConditionWithPreviousRuleStep.HasValue)
                {
                    ddlConditionWithPrevReulStep.SelectedValue = ruleDetail.ConditionWithPreviousRuleStep.Value;
                }

                txtOrder.Text = ruleDetail.OrderId.HasValue ? ruleDetail.OrderId.Value.ToString() : "";
                
                txtValidationMsg.Text = ruleDetail.ValidationMessage;

                if (ruleDetail.ValidationTypeId == 4)
                {
                    txtCustomScript.Text = ruleDetail.CustomScript;
                    ShowCustomValidationItems(true);
                }

                if (ruleDetail.ValidationTypeId == 2 || ruleDetail.ValidationTypeId == 5)
                {
                    ShowCompareValidatorItems(true);

                    txtComparisonPercent.Text = ruleDetail.ComparisonPercent.ToString();

                    if (ruleDetail.ComparisonOperatorTypeId.HasValue)
                    {
                        ddlComparisonOperatorType.SelectedValue = ruleDetail.ComparisonOperatorTypeId.Value;
                    }

                    if (ruleDetail.ComparisonValueDataTypeId.HasValue)
                    {
                        chkIsFixedComparison.Checked = true;
                        txtComparisonValue.Text = ruleDetail.ComparisonValue;
                        ddlComparisonValueDataType.SelectedValue = ruleDetail.ComparisonValueDataTypeId.Value;
                        ShowFixedComparisonItems(true);
                        ShowColumnComparisonItems(false);
                    }
                    else
                    {
                        ShowFixedComparisonItems(false);
                        ShowColumnComparisonItems(true);

                        if (ruleDetail.YearId.HasValue)
                        {
                            ddlYear.SelectedValue = ruleDetail.YearId.Value;
                        }

                        if (ruleDetail.ClientId.HasValue)
                        {
                            ddlClient.SelectedValue = ruleDetail.ClientId.Value;
                        }

                        if (ruleDetail.ComparisonColumnId.HasValue)
                        {
                            var tableId = new AppColumnService().GetById(ruleDetail.ComparisonColumnId.Value).AppTableId;
                            ddlTable.SelectedValue = tableId;

                            PopulateRowAndColumn();

                            ddlColumn.SelectedValue = ruleDetail.ComparisonColumnId.Value;
                        }

                        if (ruleDetail.ComparisonRowId.HasValue)
                        {
                            ddlRow.SelectedValue = ruleDetail.ComparisonRowId.Value;
                        }

                        if (ruleDetail.ComparisonDatasetId.HasValue)
                        {
                            ddlDataSet.SelectedValue = ruleDetail.ComparisonDatasetId.Value;
                        }
                    }
                }
            }
        }

        private void chkIsFixedComparison_MouseCaptureChanged(object sender, EventArgs e)
        {
            ShowFixedComparisonItems(chkIsFixedComparison.Checked);
            ShowColumnComparisonItems(!chkIsFixedComparison.Checked);
        }

        private void ShowCompareValidatorItems(bool show, bool isFixedComparison = true)
        {
            lblComparisonOperatorType.Visible = ddlComparisonOperatorType.Visible = show;
            lblComparisonPercent.Visible = txtComparisonPercent.Visible = show;
            lblIsFixedComparison.Visible = chkIsFixedComparison.Visible = show;
            
            ShowFixedComparisonItems(isFixedComparison);

            ShowColumnComparisonItems(!isFixedComparison);
        }

        private void ShowCustomValidationItems(bool show)
        {
            lblCustomValidationScript.Visible = txtCustomScript.Visible = show;
        }

        private void ShowFixedComparisonItems(bool show)
        {
            chkIsFixedComparison.Checked = show;
            lblComparisonValueDataType.Visible = lblComparisonValue.Visible = show;
            ddlComparisonValueDataType.Visible = txtComparisonValue.Visible = show;
        }

        private void ShowColumnComparisonItems(bool show)
        {
            lblColumn.Visible = lblDataset.Visible = lblRow.Visible = lblTable.Visible = show;
            ddlColumn.Visible = ddlDataSet.Visible = ddlRow.Visible = ddlTable.Visible = show;
            ddlYear.Visible = lblYear.Visible = ddlClient.Visible = lblClient.Visible = show;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Save changes", "Rule Detail Step", MessageBoxButtons.OKCancel);

            if (result == DialogResult.Cancel)
            {
                return;
            }
            var msg = CheckValidation();
            if (msg.Length > 5)
            {
                MessageBox.Show(msg);
                return;
            }

            var ruleDetail = new AppRuleDetail
            {
                AppRuleDetailId = selectedRuleDetailId,
                OrderId = txtOrder.Text.ToNullableInt(),
                CustomScript = txtCustomScript.Text,
                ValidationMessage = txtValidationMsg.Text,
                ValidationTypeId = ((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key,
                AppRuleId = selectedRuleId,                
            };
            
            if (txtComparisonPercent.Text.ToInt() > 0)
            {
                ruleDetail.ComparisonPercent = txtComparisonPercent.Text.ToInt();
            }

            if (((KeyValuePair<int, string>)ddlConditionWithPrevReulStep.SelectedItem).Key > 0)
            {
                ruleDetail.ConditionWithPreviousRuleStep = ((KeyValuePair<int, string>)ddlConditionWithPrevReulStep.SelectedItem).Key;
            }

            if (((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key == 2 || ((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key == 5)
            {
                if (((KeyValuePair<int, string>)ddlComparisonOperatorType.SelectedItem).Key > 0)
                {
                    ruleDetail.ComparisonOperatorTypeId = ((KeyValuePair<int, string>)ddlComparisonOperatorType.SelectedItem).Key;
                }

                if (chkIsFixedComparison.Checked)
                {
                    if (((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key > 0 || ((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key < 5)
                    {
                        ruleDetail.ComparisonValueDataTypeId = ((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key;
                    }
                    
                    ruleDetail.ComparisonValue = txtComparisonValue.Text.Trim();
                }
                else
                {
                    if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
                    {
                        ruleDetail.YearId = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
                    }
                    
                    if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
                    {
                        ruleDetail.ClientId = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
                    }

                    if (((KeyValuePair<int, string>)ddlColumn.SelectedItem).Key > 0)
                    {
                        ruleDetail.ComparisonColumnId = ((KeyValuePair<int, string>)ddlColumn.SelectedItem).Key;
                    }

                    if (((KeyValuePair<int, string>)ddlRow.SelectedItem).Key > 0)
                    {
                        ruleDetail.ComparisonRowId = ((KeyValuePair<int, string>)ddlRow.SelectedItem).Key;
                    }

                    if (ddlDataSet.SelectedItem != null && ((KeyValuePair<int, string>)ddlDataSet.SelectedItem).Key > 0)
                    {
                        ruleDetail.ComparisonDatasetId = ((KeyValuePair<int, string>)ddlDataSet.SelectedItem).Key;
                    }
                }
            }

            if (new AppRuleDetailService().Save(ruleDetail))
            {
                MessageBox.Show("Rule step was saved successfully.");
            }
            else
            {
                MessageBox.Show("Rule step was NOT saved. Please correct errors and try again.");
            }
        }

        private string CheckValidation()
        {
            StringBuilder sb = new StringBuilder();

            if (((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key < 1)
            {
                sb.Append("Validation Type is required\n");
            }

            if (((KeyValuePair<int, string>)ddlValidationType.SelectedItem).Key == 2)
            {
                if (((KeyValuePair<int, string>)ddlComparisonOperatorType.SelectedItem).Key < 1)
                {
                    sb.Append("Comparison Operator Type is required\n");
                }

                if (chkIsFixedComparison.Checked)
                {
                    if (((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key < 1 || ((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key == 5)
                    {
                        sb.Append("Select right Comparison Value Data Type\n");
                    }
                    if (string.IsNullOrWhiteSpace(txtComparisonValue.Text))
                    {
                        sb.Append("Comparison Value is required\n");
                    }
                    else
                    {
                        if (((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key == 2)
                        {
                            if (txtComparisonValue.Text.ToNullableDecimal() == null)
                            {
                                sb.Append("Comparison Value must be decimal type\n");
                            }
                        }

                        if (((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key == 3)
                        {
                            if (txtComparisonValue.Text.ToNullableInt() == null)
                            {
                                sb.Append("Comparison Value must be Int type\n");
                            }
                        }

                        if (((KeyValuePair<int, string>)ddlComparisonValueDataType.SelectedItem).Key == 4)
                        {
                            if (txtComparisonValue.Text.ToNullableDateTime() == null || txtComparisonValue.Text.ToNullableDateTime() == DateTime.MinValue)
                            {
                                sb.Append("Comparison Value must be Date type\n");
                            }
                        }
                    }
                }
                else
                {
                    if (((KeyValuePair<int, string>)ddlColumn.SelectedItem).Key < 1)
                    {
                        sb.Append("At least Column must be selected\n");
                    }
                }
            }


            return sb.ToString();
        }

        private void ddlYear_SelectionChangeCommitted(object sender, EventArgs e)
        {
            PopulateTable();
            PopulateRowAndColumn();
        }

        private void ddlClient_SelectionChangeCommitted(object sender, EventArgs e)
        {
            PopulateTable();
            PopulateRowAndColumn();
        }

        private void ddlTable_SelectionChangeCommitted(object sender, EventArgs e)
        {

            PopulateRowAndColumn();
        }

        private void PopulateRowAndColumn()
        {
            int tableId = ((KeyValuePair<int, string>)ddlTable.SelectedItem).Key;

            if (tableId > 0)
            {
                var service = new AppColumnService();

                Utility.BindComboBox(service.FindBy(x => x.AppTableId == tableId && (x.Year == null
                    || x.Year == ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key) && (x.ClientId == null
                    || x.ClientId == ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key))
                    .Select(x => new KeyValuePair<int, string>(x.AppColumnId, x.Name))
                    .ToList(), ddlColumn);

                var rowService = new AppRowService();

                Utility.BindComboBox(rowService.FindBy(x => x.AppTableId == tableId && (x.Year == null
                || x.Year == ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key) && (x.ClientId == null
                || x.ClientId == ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key))
                .Select(x => new KeyValuePair<int, string>(x.AppRowId, x.Name))
                .ToList(), ddlRow);
            }
        }
    }
}
