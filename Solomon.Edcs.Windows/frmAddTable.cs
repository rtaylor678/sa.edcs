﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Solomon.Edcs.Windows
{
    public partial class frmAddTable : Form
    {
        public bool newTable = false;
        public int selectedTable = 0;

        public frmAddTable()
        {
            InitializeComponent();
        }

        private void btnAddTable_Click(object sender, EventArgs e)
        {
            if (newTable)
            {
                AddTableProcess();
            }
            else
            {                
                UpdateTableProcess();
            }
        }

        private void frmAddTable_Load(object sender, EventArgs e)
        {
            BindDataSource();
            if (newTable)
            {
                btnAddTable.Text = "Add Table";
            }
            else
            {
                btnAddTable.Text = "Update Table";
                PopulateForm();
            }

        }

        private void AddTableProcess()
        {
            if (string.IsNullOrWhiteSpace(txtTableName.Text) || ddlStudy.SelectedItem == null)
            {
                MessageBox.Show("Table name and Study are required fields.", "Add Table");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to add Table", "Add Table", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppTable table = new AppTable
                    {
                        AppTableId = selectedTable,
                        Name = txtTableName.Text,
                        Study = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Key,
                        DisplayName = txtTableDisplayName.Text
                    };

                    if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
                    {
                        table.Year = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
                    }

                    if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
                    {
                        table.Client = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
                    }

                    if (((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key > 0)
                    {
                        table.Status = ((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key;
                    }

                    if (ddlIsFixedRows.SelectedItem != null && ddlIsFixedRows.SelectedItem.ToString() == "Yes")
                    {
                        table.IsFixedNoOfRows = true;
                    }
                    else
                    {
                        table.IsFixedNoOfRows = false;
                    }

                    AppTableService service = new AppTableService();
                    service.Add(table);
                }
            }
        }

        private void UpdateTableProcess()
        {
            if (string.IsNullOrWhiteSpace(txtTableName.Text) || ddlStudy.SelectedItem == null)
            {
                MessageBox.Show("Table name and Study are required fields.", "Update Table");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to Update Table", "Update Table", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppTable table = new AppTable
                    {
                        AppTableId = selectedTable,
                        Name = txtTableName.Text,
                        Study = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Key,
                        DisplayName = txtTableDisplayName.Text
                    };

                    if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
                    {
                        table.Year = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
                    }

                    if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
                    {
                        table.Client = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
                    }

                    if (((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key > 0)
                    {
                        table.Status = ((KeyValuePair<int, string>)ddlStatus.SelectedItem).Key;
                    }

                    if (ddlIsFixedRows.SelectedItem != null && ddlIsFixedRows.SelectedItem.ToString() == "Yes")
                    {
                        table.IsFixedNoOfRows = true;
                    }
                    else
                    {
                        table.IsFixedNoOfRows = false;
                    }

                    AppTableService service = new AppTableService();
                    service.UpdateTable(table);
                }
            }
        }

        private void PopulateForm()
        {
            var table = new AppTableService().GetById(selectedTable);

            if (table != null)
            {
                if (table.Client.HasValue)
                {
                    ddlClient.SelectedValue = table.Client.Value;
                }
                
                if (table.Year.HasValue)
                {
                    ddlYear.SelectedValue = table.Year.Value;
                }
                
                if (table.Status.HasValue)
                {
                    ddlStatus.SelectedValue = table.Status.Value;
                }

                if (table.IsFixedNoOfRows.HasValue)
                {
                    if (table.IsFixedNoOfRows.Value == true)
                    {
                        ddlIsFixedRows.SelectedItem = "Yes";
                    }
                    else
                    {
                        ddlIsFixedRows.SelectedItem = "No";
                    }
                }

                ddlStudy.SelectedValue = table.Study;
                txtTableName.Text = table.Name;
                txtTableDisplayName.Text = table.DisplayName;
            }
        }

        private void BindDataSource()
        {
            var service = new AppLookupService();

            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Study").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlStudy);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "StudyYear").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlYear);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "TableStatus").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlStatus);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Client").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlClient);
        }        
    }
}
