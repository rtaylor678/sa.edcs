﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.Edcs.Utility;

namespace Solomon.Edcs.Windows
{
    public partial class frmRuleAssociation : Form
    {
        public frmRuleAssociation()
        {
            InitializeComponent();
        }

        private void btnDeleteAssociation_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to DELETE this Rule Association?", "Rule Association", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                foreach (DataGridViewRow row in dgRuleAssociations.SelectedRows)
                {
                    int associationId = row.Cells[0].Value.ToInt();
                    if (associationId > 0)
                    {
                        new AppRuleAssociationService().Delete(new AppRuleAssociation { AppRuleAssociationId = associationId });
                    }
                }
                MessageBox.Show("Rule Association(s) deleted successfully.");
            }
        }

        private void frmRuleAssociation_Load(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void BindDataSource()
        {
            Utility.BindComboBox(new AppRuleService().GetAll(x => x.IsActive == true).Select(x => new KeyValuePair<int, string>(x.AppRuleId, x.Name + " - " + x.DisplayName)).ToList(), ddlRules);
        }

        private void ddlRules_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int ruleId = ((KeyValuePair<int, string>)ddlRules.SelectedItem).Key;
            if (ruleId > 0)
            {
                BindingSource source = new BindingSource();

                source.DataSource = new AppRuleAssociationService().GetAll(x => x.AppRuleId == ruleId).ToList();

                dgRuleAssociations.DataSource = source;
            }
        }
    }
}
