﻿namespace Solomon.Edcs.Windows
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ddlStudy = new System.Windows.Forms.ComboBox();
            this.ddlYear = new System.Windows.Forms.ComboBox();
            this.ddlClient = new System.Windows.Forms.ComboBox();
            this.lstTables = new System.Windows.Forms.ListBox();
            this.ctxMnuListTables = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddTable = new System.Windows.Forms.ToolStripMenuItem();
            this.EditTable = new System.Windows.Forms.ToolStripMenuItem();
            this.DesignTable = new System.Windows.Forms.ToolStripMenuItem();
            this.RowTableDesign = new System.Windows.Forms.ToolStripMenuItem();
            this.GetTableData = new System.Windows.Forms.ToolStripMenuItem();
            this.AssignValidationRule = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ctxMnuListTables.SuspendLayout();
            this.SuspendLayout();
            // 
            // ddlStudy
            // 
            this.ddlStudy.FormattingEnabled = true;
            this.ddlStudy.Location = new System.Drawing.Point(12, 49);
            this.ddlStudy.Name = "ddlStudy";
            this.ddlStudy.Size = new System.Drawing.Size(121, 21);
            this.ddlStudy.TabIndex = 0;
            this.ddlStudy.Text = "Select a Study";
            this.ddlStudy.SelectedIndexChanged += new System.EventHandler(this.ddlStudy_SelectedIndexChanged);
            // 
            // ddlYear
            // 
            this.ddlYear.FormattingEnabled = true;
            this.ddlYear.Location = new System.Drawing.Point(150, 49);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(121, 21);
            this.ddlYear.TabIndex = 1;
            this.ddlYear.Text = "Select a Year";
            this.ddlYear.SelectedIndexChanged += new System.EventHandler(this.ddlYear_SelectedIndexChanged);
            // 
            // ddlClient
            // 
            this.ddlClient.FormattingEnabled = true;
            this.ddlClient.Location = new System.Drawing.Point(287, 49);
            this.ddlClient.Name = "ddlClient";
            this.ddlClient.Size = new System.Drawing.Size(121, 21);
            this.ddlClient.TabIndex = 2;
            this.ddlClient.Text = "Select a Client";
            this.ddlClient.SelectedIndexChanged += new System.EventHandler(this.ddlClient_SelectedIndexChanged);
            // 
            // lstTables
            // 
            this.lstTables.ContextMenuStrip = this.ctxMnuListTables;
            this.lstTables.FormattingEnabled = true;
            this.lstTables.Location = new System.Drawing.Point(12, 76);
            this.lstTables.Name = "lstTables";
            this.lstTables.Size = new System.Drawing.Size(396, 303);
            this.lstTables.TabIndex = 3;
            // 
            // ctxMnuListTables
            // 
            this.ctxMnuListTables.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddTable,
            this.EditTable,
            this.DesignTable,
            this.RowTableDesign,
            this.GetTableData,
            this.AssignValidationRule});
            this.ctxMnuListTables.Name = "ctxMnuListTables";
            this.ctxMnuListTables.ShowCheckMargin = true;
            this.ctxMnuListTables.Size = new System.Drawing.Size(216, 136);
            this.ctxMnuListTables.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ctxMnuListTables_MouseClick);
            // 
            // AddTable
            // 
            this.AddTable.Name = "AddTable";
            this.AddTable.Size = new System.Drawing.Size(215, 22);
            this.AddTable.Text = "Add Table";
            // 
            // EditTable
            // 
            this.EditTable.Name = "EditTable";
            this.EditTable.Size = new System.Drawing.Size(215, 22);
            this.EditTable.Text = "Edit Table";
            // 
            // DesignTable
            // 
            this.DesignTable.Name = "DesignTable";
            this.DesignTable.Size = new System.Drawing.Size(215, 22);
            this.DesignTable.Text = "Design Table Columns";
            // 
            // RowTableDesign
            // 
            this.RowTableDesign.Name = "RowTableDesign";
            this.RowTableDesign.Size = new System.Drawing.Size(215, 22);
            this.RowTableDesign.Text = "Design Table Rows";
            // 
            // GetTableData
            // 
            this.GetTableData.Name = "GetTableData";
            this.GetTableData.Size = new System.Drawing.Size(215, 22);
            this.GetTableData.Text = "Select Data";
            // 
            // AssignValidationRule
            // 
            this.AssignValidationRule.Name = "AssignValidationRule";
            this.AssignValidationRule.Size = new System.Drawing.Size(215, 22);
            this.AssignValidationRule.Text = "Assign Validation Rule";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Study";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(150, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Year";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Client";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 482);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstTables);
            this.Controls.Add(this.ddlClient);
            this.Controls.Add(this.ddlYear);
            this.Controls.Add(this.ddlStudy);
            this.Name = "Main";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ctxMnuListTables.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlStudy;
        private System.Windows.Forms.ComboBox ddlYear;
        private System.Windows.Forms.ComboBox ddlClient;
        private System.Windows.Forms.ListBox lstTables;
        private System.Windows.Forms.ContextMenuStrip ctxMnuListTables;
        private System.Windows.Forms.ToolStripMenuItem AddTable;
        private System.Windows.Forms.ToolStripMenuItem DesignTable;
        private System.Windows.Forms.ToolStripMenuItem GetTableData;
        private System.Windows.Forms.ToolStripMenuItem EditTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem RowTableDesign;
        private System.Windows.Forms.ToolStripMenuItem AssignValidationRule;
    }
}

