﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.Edcs.Utility;

namespace Solomon.Edcs.Windows
{
    public partial class frmDataTable : Form
    {
        public int selectedTable = -1;
        //public int datasetId = 1;
        public int selectedYear = -1;
        public int selectedClient = -1;
        public int selectedDataset = -1;

        public frmDataTable()
        {
            InitializeComponent();
            BindDataSource();
        }

        private void frmDataTable_Load(object sender, EventArgs e)
        {
            if (Global.SelectedStudyId > 0)
            {
                ddlStudy.SelectedValue = Global.SelectedStudyId;
                if (selectedTable > 0)
                {
                    BindTableSource(Global.SelectedStudyId);
                    ddlTable.SelectedValue = selectedTable;
                }
            }
            
        }
        private void BindTableSource(int studyId)
        {
            Utility.BindComboBox(new AppTableService().GetAll(x => x.Study == studyId).Select(x => new KeyValuePair<int, string>(x.AppTableId, x.Name + " - " + x.DisplayName)).ToList(), ddlTable, false);
        }
        private void PopulateDataTable()
        {
            ResetSelectedValues();
            
            if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
            {
                selectedYear = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
            }

            if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
            {
                selectedClient = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
            }

            if (ddlDataset.SelectedItem != null && ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key > 0)
            {
                selectedDataset = ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key;
            }

            var appTable = new AppTableService().GetById(selectedTable);

            if (appTable.IsFixedNoOfRows.HasValue && appTable.IsFixedNoOfRows == true)
            {
                dgColumns.AllowUserToAddRows = false;
            }

            var data = new AppTableService().GetTableData(selectedTable, selectedYear, selectedClient, selectedDataset);

            var columns = new AppColumnService().GetColumnList(selectedTable, selectedYear, selectedClient);

            DataTable table = GetDataPivotTable(data, columns);

            BindingSource source = new BindingSource();
            source.DataSource = table;

            if (appTable.IsFixedNoOfRows.HasValue && appTable.IsFixedNoOfRows == true)
            {
                dgColumns.DataSource = table;
            }
            else
            {
                dgColumns.DataSource = source;
            }
        }

        public DataTable GetDataPivotTable(IList<GetTableDataCellValue_Result> data, IList<AppColumn> columns)
        {
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("RowId");

            foreach (var col in columns)
            {
                dataTable.Columns.Add(col.Name);
            }

            var dataSets = data.Select(x => new { x.AppDataSetId.Value}).Distinct().ToList();

            foreach (var datasetId in dataSets)
            {
                var rows = data.Where(x => x.AppDataSetId.Value == datasetId.Value).Select(x => new {x.AppDataSetId, x.RowId }).Distinct().ToList();
                foreach (var row in rows)
                {
                    var values = new object[columns.Count + 1];

                    values[0] = row.RowId;
                    for (int i = 0; i < columns.Count; i++)
                    {
                        var result = data.Where(x => x.RowId == row.RowId & x.AppDataSetId == row.AppDataSetId && x.ColumnName == columns[i].Name).FirstOrDefault();
                        if (result != null)
                        {
                            values[i + 1] = result.CellValue;
                        }
                    }
                    dataTable.Rows.Add(values);
                }
            }

            return dataTable;
        }
        private void BindDataSource()
        {
            var service = new AppLookupService();

            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "StudyYear").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlYear);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Client").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlClient);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Study").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlStudy, false);
        }

        private void ddlYear_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataSet();
        }

        private void ddlClient_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataSet();
        }

        private void SetDataSet()
        {
            ddlDataset.DataSource = null;
            ddlDataset.Enabled = false;

            selectedYear = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
            selectedClient = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;

            if (selectedYear > 0 && selectedClient > 0)
            {
                var dataSets = new AppDataSetService().GetAll(x => x.Study == Global.SelectedStudyId && x.Year == selectedYear && x.Client == selectedClient).Select(y => new KeyValuePair<int, string>(y.AppDataSetId, y.AppDataSetId.ToString())).ToList();

                if (dataSets != null && dataSets.Count > 0)
                {
                    Utility.BindComboBox(dataSets, ddlDataset);
                    ddlDataset.Enabled = true;
                }                
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ddlDataset.SelectedItem != null && ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key > 0)
            {
                var row = dgColumns.CurrentRow;

                int rowId = row.Cells["RowId"].Value.ToInt();
                                
                var columns = new AppColumnService().GetColumnList(selectedTable, selectedYear, selectedClient);

                TableData data = new TableData();

                foreach (var col in columns)
                {
                    if (rowId > 0)
                    {
                        if (col.DataTypeId == 1)
                        {
                            data.AppCellValueTextList.Add(new AppCellValueText
                            {
                                AppDataSetId = ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key,
                                AppColumnId = col.AppColumnId,
                                CellValue = Convert.IsDBNull(row.Cells[col.Name].Value) ? null : (string)row.Cells[col.Name].Value,
                                AppRowId = rowId
                            });
                        
                        
                        }
                        if (col.DataTypeId == 2)
                        {
                            decimal decimalValue = 0;
                            
                            if (!Convert.IsDBNull(row.Cells[col.Name].Value))
                            {
                                decimal.TryParse(row.Cells[col.Name].Value.ToString(), out decimalValue);
                            }

                            var cellDecimal = new AppCellValueDecimal
                            {
                                AppDataSetId = ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key,
                                AppColumnId = col.AppColumnId,
                                AppRowId = rowId
                            };

                            if (decimalValue > 0)
                            {
                                cellDecimal.CellValue = decimalValue;
                            }

                           data.AppCellValueDecimalList.Add(cellDecimal);
                        }

                        if (col.DataTypeId == 3)
                        {
                             int intValue = 0;
                            
                            if (!Convert.IsDBNull(row.Cells[col.Name].Value))
                            {
                                int.TryParse(row.Cells[col.Name].Value.ToString(), out intValue);
                            }

                            var cellInt = new AppCellValueInt
                            {
                                AppDataSetId = ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key,
                                AppColumnId = col.AppColumnId,
                                AppRowId = rowId
                            };

                            if (intValue > 0)
                            {
                                cellInt.CellValue = intValue;
                            }

                            data.AppCellValueIntList.Add(cellInt);
                        }

                        if (col.DataTypeId == 4)
                        {
                            DateTime dateTimeValue = DateTime.MinValue;

                            if (!Convert.IsDBNull(row.Cells[col.Name].Value))
                            {
                                DateTime.TryParse(row.Cells[col.Name].Value.ToString(), out dateTimeValue);
                            }

                            var cellDateTime = new AppCellValueDateTime
                            {
                                AppDataSetId = ((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key,
                                AppColumnId = col.AppColumnId,
                                AppRowId = rowId
                            };

                            if (dateTimeValue > DateTime.MinValue)
                            {
                                cellDateTime.CellValue = dateTimeValue;
                            }

                            data.AppCellValueDateTimeList.Add(cellDateTime);
                        }

                    }
                }

                var tableDataService = new TableDataService();

                string msg = tableDataService.CheckValidation(data);

                if (msg.Length < 5)
                {
                    if (tableDataService.Save(data))
                    {
                        MessageBox.Show("Data Saved successfully.", "Save Data");
                    }
                    else
                    {
                        MessageBox.Show("Data was NOT Saved. Please check input data", "Save Data");
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show(string.Format("Following Validation issue found \n\n\n{0}. \n\n\nDo you still want to save?", msg), "Save Data", MessageBoxButtons.OKCancel);
                    if (result == DialogResult.OK)
                    {
                        if (tableDataService.Save(data))
                        {
                            MessageBox.Show("Data Saved successfully.", "Save Data");
                        }
                        else
                        {
                            MessageBox.Show("Data was NOT Saved. Please check input data", "Save Data");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Dataset must be selected for saving data.", "Save Data");
            }
        }


        private void btnCreateDataset_Click(object sender, EventArgs e)
        {
            if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key < 0 || ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key < 0)
            {
                MessageBox.Show("Year and Client are required fields.", "Add Dataset");
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to Add Dataset", "Add Dataset", MessageBoxButtons.OKCancel);

                if (result == DialogResult.OK)
                {
                    AppDataSet dataset = new AppDataSet
                    {
                        Study = Global.SelectedStudyId,
                        Year = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key,
                        Client = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key
                    };

                    int submissionId = 0;

                    int.TryParse(txtSubmissionId.Text, out submissionId);

                    if (submissionId > 0)
                    {
                        dataset.Submission = submissionId;
                    }

                    if (!string.IsNullOrWhiteSpace(txtCriteria.Text))
                    {
                        dataset.Criteria = txtCriteria.Text;
                    }

                    var service = new AppDataSetService();
                    if (service.FindBy(x => x.Client == selectedClient && x.Criteria == dataset.Criteria && x.Submission == dataset.Submission && x.Year == dataset.Year).Count() > 0)
                    {
                        MessageBox.Show("Dataset already exist. Please try different combination", "Add Dataset");
                        return;
                    }

                    new AppDataSetService().Add(dataset);                    
                }
            }
        }

        private void ddlDataset_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key > 0)
            {
                var appDataset = new AppDataSetService().GetById(((KeyValuePair<int, string>)ddlDataset.SelectedItem).Key);
                txtSubmissionId.Text = appDataset.Submission.HasValue ? appDataset.Submission.Value.ToString() : "";
                txtCriteria.Text = appDataset.Criteria;
            }
            PopulateDataTable();
        }

        private void ResetSelectedValues()
        {
            selectedYear = -1;
            selectedClient = -1;
            selectedDataset = -1;
        }

        private void ddlStudy_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Global.SelectedStudyId = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Key;
            BindTableSource(Global.SelectedStudyId);
            SetDataSet();
            dgColumns.DataSource = null;
        }

        private void ddlTable_SelectionChangeCommitted(object sender, EventArgs e)
        {
            selectedTable = ((KeyValuePair<int, string>)ddlTable.SelectedItem).Key;
            PopulateDataTable();
        }
    }
}
