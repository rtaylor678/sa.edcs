﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.Edcs.Utility;

namespace Solomon.Edcs.Windows
{
    public partial class frmValidationRule : Form
    {
        public int selectedTable = -1;
        public int selectedStudyId = -1;
        //public int datasetId = 1;
        public string table = "";
        public string study = "";        

        public frmValidationRule()
        {
            InitializeComponent();
            BindDataSource();
        }

        private void frmDataTable_Load(object sender, EventArgs e)
        {
            PopulateDataTable();
            txtStudy.Text = study;
            txtTable.Text = table;
        }

        private void PopulateDataTable()
        {
            dgColumns.DataSource = new AppColumnService().GetAll(x => x.AppTableId == selectedTable).Select(y => new { y.AppColumnId, y.Name, y.DisplayName }).ToList();            
            dgRows.DataSource = new AppRowService().GetAll(x => x.AppTableId == selectedTable).Select(y => new { y.AppRowId, y.Name }).ToList();
            dgRows.ClearSelection();
            dgRules.DataSource = new AppRuleService().GetAll().Select(x => new { x.AppRuleId, x.Name, x.DisplayName }).ToList();            
        }

        private void PopulateColumns()
        {
            dgColumns.DataSource = new AppColumnService().GetAll(x => x.AppTableId == selectedTable).Select(y => new {y.AppColumnId, y.Name, y.DisplayName }).ToList();
        }

        private DataTable GetDataPivotTable(IList<GetTableDataCellValue_Result> data, IList<AppColumn> columns)
        {
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("RowId");

            foreach (var col in columns)
            {
                dataTable.Columns.Add(col.Name);
            }

            var dataSets = data.Select(x => new { x.AppDataSetId.Value}).Distinct().ToList();

            foreach (var datasetId in dataSets)
            {
                var rows = data.Where(x => x.AppDataSetId.Value == datasetId.Value).Select(x => new {x.AppDataSetId, x.RowId }).Distinct().ToList();
                foreach (var row in rows)
                {
                    var values = new object[columns.Count + 1];

                    values[0] = row.RowId;
                    for (int i = 0; i < columns.Count; i++)
                    {
                        var result = data.Where(x => x.RowId == row.RowId & x.AppDataSetId == row.AppDataSetId && x.ColumnName == columns[i].Name).FirstOrDefault();
                        if (result != null)
                        {
                            values[i + 1] = result.CellValue;
                        }
                    }
                    dataTable.Rows.Add(values);
                }
            }

            return dataTable;
        }
        private void BindDataSource()
        {
            var service = new AppLookupService();

            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "StudyYear").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlYear);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Client").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlClient);
        }

        private void ddlYear_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

        private void ddlClient_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
        }

        

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Create Rule Association", "Rule Association", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                int? selectedYear = null;
                if (((KeyValuePair<int, string>)ddlYear.SelectedItem).Key > 0)
                {
                    selectedYear = ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key;
                }
                int? selectedClient = null;
                if (((KeyValuePair<int, string>)ddlClient.SelectedItem).Key > 0)
                {
                    selectedClient = ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key;
                }
                string validationMsg = txtValidationMsg.Text;

                List<AppRuleAssociation> ruleAssociations = new List<AppRuleAssociation>();
                
                foreach (DataGridViewRow col in dgColumns.SelectedRows)
                {                    
                    foreach (DataGridViewRow rule in dgRules.SelectedRows)
                    {
                        foreach (DataGridViewRow row in dgRows.SelectedRows)
                        {
                            ruleAssociations.Add(new AppRuleAssociation
                            {
                                AppColumnId = col.Cells[0].Value.ToInt(),
                                AppRuleId = rule.Cells[0].Value.ToInt(),
                                AppRowId = row.Cells[0].Value.ToInt(),
                                ClientId = selectedClient,
                                YearId = selectedYear,
                                Message = validationMsg
                            });
                        }

                        if (dgRows.SelectedRows.Count == 0)
                        {
                            ruleAssociations.Add(new AppRuleAssociation
                            {
                                AppColumnId = col.Cells[0].Value.ToInt(),
                                AppRuleId = rule.Cells[0].Value.ToInt(),
                                ClientId = selectedClient,
                                YearId = selectedYear,
                                Message = validationMsg
                            });
                        }
                    }
                }

                if (new AppRuleAssociationService().Add(ruleAssociations))
                {
                    MessageBox.Show("Rule Association created successfully.");
                }
                else
                {
                    MessageBox.Show("Rule Association NOT created. Please make sure all selection(s) are correct and try again.");
                }
            }
            else
            {
                dgRules.ClearSelection();
                dgRows.ClearSelection();
                dgColumns.ClearSelection();
            }
        }

        private void dgDataTable_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //var table = dgDataTable.DataSource;
        }

        private void btnCreateDataset_Click(object sender, EventArgs e)
        {
            new frmRuleManager().Show();
        }

        private void btnViewRuleAssociation_Click(object sender, EventArgs e)
        {
            new frmRuleAssociation().Show();
        }
    }
}
