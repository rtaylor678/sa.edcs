﻿namespace Solomon.Edcs.Windows
{
    partial class frmRuleDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.dgRuleDetails = new System.Windows.Forms.DataGridView();
            this.btnEditRuleStep = new System.Windows.Forms.Button();
            this.btnAddNewRuleStep = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgRuleDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(121, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Validation Rule Detail:";
            // 
            // dgRuleDetails
            // 
            this.dgRuleDetails.AllowUserToDeleteRows = false;
            this.dgRuleDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRuleDetails.Location = new System.Drawing.Point(118, 51);
            this.dgRuleDetails.MultiSelect = false;
            this.dgRuleDetails.Name = "dgRuleDetails";
            this.dgRuleDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgRuleDetails.Size = new System.Drawing.Size(786, 295);
            this.dgRuleDetails.TabIndex = 46;
            // 
            // btnEditRuleStep
            // 
            this.btnEditRuleStep.Location = new System.Drawing.Point(119, 352);
            this.btnEditRuleStep.Name = "btnEditRuleStep";
            this.btnEditRuleStep.Size = new System.Drawing.Size(106, 23);
            this.btnEditRuleStep.TabIndex = 49;
            this.btnEditRuleStep.Text = "Edit Rule Step";
            this.btnEditRuleStep.UseVisualStyleBackColor = true;
            this.btnEditRuleStep.Click += new System.EventHandler(this.btnEditRuleStep_Click);
            // 
            // btnAddNewRuleStep
            // 
            this.btnAddNewRuleStep.Location = new System.Drawing.Point(257, 352);
            this.btnAddNewRuleStep.Name = "btnAddNewRuleStep";
            this.btnAddNewRuleStep.Size = new System.Drawing.Size(123, 23);
            this.btnAddNewRuleStep.TabIndex = 50;
            this.btnAddNewRuleStep.Text = "Add New Rule Step";
            this.btnAddNewRuleStep.UseVisualStyleBackColor = true;
            this.btnAddNewRuleStep.Click += new System.EventHandler(this.btnAddNewRuleStep_Click);
            // 
            // frmRuleDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 390);
            this.Controls.Add(this.btnAddNewRuleStep);
            this.Controls.Add(this.btnEditRuleStep);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dgRuleDetails);
            this.Name = "frmRuleDetails";
            this.Text = "Rule Details";
            this.Load += new System.EventHandler(this.frmRuleDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRuleDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgRuleDetails;
        private System.Windows.Forms.Button btnEditRuleStep;
        private System.Windows.Forms.Button btnAddNewRuleStep;
    }
}