﻿namespace Solomon.Edcs.Windows
{
    partial class frmAddEditRuleDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlValidationType = new System.Windows.Forms.ComboBox();
            this.ddlConditionWithPrevReulStep = new System.Windows.Forms.ComboBox();
            this.txtOrder = new System.Windows.Forms.TextBox();
            this.ddlComparisonOperatorType = new System.Windows.Forms.ComboBox();
            this.txtComparisonValue = new System.Windows.Forms.TextBox();
            this.ddlComparisonValueDataType = new System.Windows.Forms.ComboBox();
            this.ddlTable = new System.Windows.Forms.ComboBox();
            this.ddlColumn = new System.Windows.Forms.ComboBox();
            this.ddlRow = new System.Windows.Forms.ComboBox();
            this.ddlDataSet = new System.Windows.Forms.ComboBox();
            this.txtCustomScript = new System.Windows.Forms.TextBox();
            this.txtValidationMsg = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblComparisonOperatorType = new System.Windows.Forms.Label();
            this.lblComparisonValue = new System.Windows.Forms.Label();
            this.lblCustomValidationScript = new System.Windows.Forms.Label();
            this.lblComparisonValueDataType = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTable = new System.Windows.Forms.Label();
            this.lblRow = new System.Windows.Forms.Label();
            this.lblColumn = new System.Windows.Forms.Label();
            this.lblDataset = new System.Windows.Forms.Label();
            this.txtRuleName = new System.Windows.Forms.TextBox();
            this.txtRuleDisplayName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblIsFixedComparison = new System.Windows.Forms.Label();
            this.chkIsFixedComparison = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblClient = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ddlClient = new System.Windows.Forms.ComboBox();
            this.ddlYear = new System.Windows.Forms.ComboBox();
            this.txtStudy = new System.Windows.Forms.TextBox();
            this.lblComparisonPercent = new System.Windows.Forms.Label();
            this.txtComparisonPercent = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ddlValidationType
            // 
            this.ddlValidationType.FormattingEnabled = true;
            this.ddlValidationType.Location = new System.Drawing.Point(170, 79);
            this.ddlValidationType.Name = "ddlValidationType";
            this.ddlValidationType.Size = new System.Drawing.Size(121, 21);
            this.ddlValidationType.TabIndex = 0;
            this.ddlValidationType.SelectionChangeCommitted += new System.EventHandler(this.ddlValidationType_SelectionChangeCommitted);
            // 
            // ddlConditionWithPrevReulStep
            // 
            this.ddlConditionWithPrevReulStep.FormattingEnabled = true;
            this.ddlConditionWithPrevReulStep.Location = new System.Drawing.Point(170, 116);
            this.ddlConditionWithPrevReulStep.Name = "ddlConditionWithPrevReulStep";
            this.ddlConditionWithPrevReulStep.Size = new System.Drawing.Size(121, 21);
            this.ddlConditionWithPrevReulStep.TabIndex = 1;
            // 
            // txtOrder
            // 
            this.txtOrder.Location = new System.Drawing.Point(170, 144);
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.Size = new System.Drawing.Size(100, 20);
            this.txtOrder.TabIndex = 2;
            // 
            // ddlComparisonOperatorType
            // 
            this.ddlComparisonOperatorType.FormattingEnabled = true;
            this.ddlComparisonOperatorType.Location = new System.Drawing.Point(170, 171);
            this.ddlComparisonOperatorType.Name = "ddlComparisonOperatorType";
            this.ddlComparisonOperatorType.Size = new System.Drawing.Size(121, 21);
            this.ddlComparisonOperatorType.TabIndex = 3;
            this.ddlComparisonOperatorType.Visible = false;
            // 
            // txtComparisonValue
            // 
            this.txtComparisonValue.Location = new System.Drawing.Point(170, 248);
            this.txtComparisonValue.Name = "txtComparisonValue";
            this.txtComparisonValue.Size = new System.Drawing.Size(100, 20);
            this.txtComparisonValue.TabIndex = 4;
            this.txtComparisonValue.Visible = false;
            // 
            // ddlComparisonValueDataType
            // 
            this.ddlComparisonValueDataType.FormattingEnabled = true;
            this.ddlComparisonValueDataType.Location = new System.Drawing.Point(170, 275);
            this.ddlComparisonValueDataType.Name = "ddlComparisonValueDataType";
            this.ddlComparisonValueDataType.Size = new System.Drawing.Size(121, 21);
            this.ddlComparisonValueDataType.TabIndex = 5;
            this.ddlComparisonValueDataType.Visible = false;
            // 
            // ddlTable
            // 
            this.ddlTable.FormattingEnabled = true;
            this.ddlTable.Location = new System.Drawing.Point(525, 124);
            this.ddlTable.Name = "ddlTable";
            this.ddlTable.Size = new System.Drawing.Size(335, 21);
            this.ddlTable.TabIndex = 6;
            this.ddlTable.Visible = false;
            this.ddlTable.SelectionChangeCommitted += new System.EventHandler(this.ddlTable_SelectionChangeCommitted);
            // 
            // ddlColumn
            // 
            this.ddlColumn.FormattingEnabled = true;
            this.ddlColumn.Location = new System.Drawing.Point(525, 151);
            this.ddlColumn.Name = "ddlColumn";
            this.ddlColumn.Size = new System.Drawing.Size(335, 21);
            this.ddlColumn.TabIndex = 7;
            this.ddlColumn.Visible = false;
            // 
            // ddlRow
            // 
            this.ddlRow.FormattingEnabled = true;
            this.ddlRow.Location = new System.Drawing.Point(525, 177);
            this.ddlRow.Name = "ddlRow";
            this.ddlRow.Size = new System.Drawing.Size(335, 21);
            this.ddlRow.TabIndex = 8;
            this.ddlRow.Visible = false;
            // 
            // ddlDataSet
            // 
            this.ddlDataSet.FormattingEnabled = true;
            this.ddlDataSet.Location = new System.Drawing.Point(525, 206);
            this.ddlDataSet.Name = "ddlDataSet";
            this.ddlDataSet.Size = new System.Drawing.Size(121, 21);
            this.ddlDataSet.TabIndex = 9;
            this.ddlDataSet.Visible = false;
            // 
            // txtCustomScript
            // 
            this.txtCustomScript.Enabled = false;
            this.txtCustomScript.Location = new System.Drawing.Point(170, 315);
            this.txtCustomScript.Name = "txtCustomScript";
            this.txtCustomScript.Size = new System.Drawing.Size(575, 20);
            this.txtCustomScript.TabIndex = 10;
            this.txtCustomScript.Visible = false;
            // 
            // txtValidationMsg
            // 
            this.txtValidationMsg.Location = new System.Drawing.Point(170, 347);
            this.txtValidationMsg.Name = "txtValidationMsg";
            this.txtValidationMsg.Size = new System.Drawing.Size(575, 20);
            this.txtValidationMsg.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Validation Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Condition With Previous Rule";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Rule Step Order";
            // 
            // lblComparisonOperatorType
            // 
            this.lblComparisonOperatorType.AutoSize = true;
            this.lblComparisonOperatorType.Location = new System.Drawing.Point(29, 174);
            this.lblComparisonOperatorType.Name = "lblComparisonOperatorType";
            this.lblComparisonOperatorType.Size = new System.Drawing.Size(133, 13);
            this.lblComparisonOperatorType.TabIndex = 15;
            this.lblComparisonOperatorType.Text = "Comparison Operator Type";
            this.lblComparisonOperatorType.Visible = false;
            // 
            // lblComparisonValue
            // 
            this.lblComparisonValue.AutoSize = true;
            this.lblComparisonValue.Location = new System.Drawing.Point(70, 251);
            this.lblComparisonValue.Name = "lblComparisonValue";
            this.lblComparisonValue.Size = new System.Drawing.Size(92, 13);
            this.lblComparisonValue.TabIndex = 16;
            this.lblComparisonValue.Text = "Comparison Value";
            this.lblComparisonValue.Visible = false;
            // 
            // lblCustomValidationScript
            // 
            this.lblCustomValidationScript.AutoSize = true;
            this.lblCustomValidationScript.Location = new System.Drawing.Point(44, 318);
            this.lblCustomValidationScript.Name = "lblCustomValidationScript";
            this.lblCustomValidationScript.Size = new System.Drawing.Size(121, 13);
            this.lblCustomValidationScript.TabIndex = 17;
            this.lblCustomValidationScript.Text = "Custom Validation Script";
            this.lblCustomValidationScript.Visible = false;
            // 
            // lblComparisonValueDataType
            // 
            this.lblComparisonValueDataType.AutoSize = true;
            this.lblComparisonValueDataType.Location = new System.Drawing.Point(17, 278);
            this.lblComparisonValueDataType.Name = "lblComparisonValueDataType";
            this.lblComparisonValueDataType.Size = new System.Drawing.Size(145, 13);
            this.lblComparisonValueDataType.TabIndex = 18;
            this.lblComparisonValueDataType.Text = "Comparison Value Data Type";
            this.lblComparisonValueDataType.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(63, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Validation Message";
            // 
            // lblTable
            // 
            this.lblTable.AutoSize = true;
            this.lblTable.Location = new System.Drawing.Point(449, 127);
            this.lblTable.Name = "lblTable";
            this.lblTable.Size = new System.Drawing.Size(70, 13);
            this.lblTable.TabIndex = 20;
            this.lblTable.Text = "Select Table ";
            this.lblTable.Visible = false;
            // 
            // lblRow
            // 
            this.lblRow.AutoSize = true;
            this.lblRow.Location = new System.Drawing.Point(457, 182);
            this.lblRow.Name = "lblRow";
            this.lblRow.Size = new System.Drawing.Size(62, 13);
            this.lblRow.TabIndex = 21;
            this.lblRow.Text = "Select Row";
            this.lblRow.Visible = false;
            // 
            // lblColumn
            // 
            this.lblColumn.AutoSize = true;
            this.lblColumn.Location = new System.Drawing.Point(444, 154);
            this.lblColumn.Name = "lblColumn";
            this.lblColumn.Size = new System.Drawing.Size(75, 13);
            this.lblColumn.TabIndex = 22;
            this.lblColumn.Text = "Select Column";
            this.lblColumn.Visible = false;
            // 
            // lblDataset
            // 
            this.lblDataset.AutoSize = true;
            this.lblDataset.Location = new System.Drawing.Point(442, 206);
            this.lblDataset.Name = "lblDataset";
            this.lblDataset.Size = new System.Drawing.Size(77, 13);
            this.lblDataset.TabIndex = 23;
            this.lblDataset.Text = "Seelct Dataset";
            this.lblDataset.Visible = false;
            // 
            // txtRuleName
            // 
            this.txtRuleName.Enabled = false;
            this.txtRuleName.Location = new System.Drawing.Point(170, 33);
            this.txtRuleName.Name = "txtRuleName";
            this.txtRuleName.Size = new System.Drawing.Size(171, 20);
            this.txtRuleName.TabIndex = 24;
            // 
            // txtRuleDisplayName
            // 
            this.txtRuleDisplayName.Enabled = false;
            this.txtRuleDisplayName.Location = new System.Drawing.Point(525, 33);
            this.txtRuleDisplayName.Name = "txtRuleDisplayName";
            this.txtRuleDisplayName.Size = new System.Drawing.Size(304, 20);
            this.txtRuleDisplayName.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(102, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Rule Name";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(422, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Rule Display Name";
            // 
            // lblIsFixedComparison
            // 
            this.lblIsFixedComparison.AutoSize = true;
            this.lblIsFixedComparison.Location = new System.Drawing.Point(61, 224);
            this.lblIsFixedComparison.Name = "lblIsFixedComparison";
            this.lblIsFixedComparison.Size = new System.Drawing.Size(101, 13);
            this.lblIsFixedComparison.TabIndex = 29;
            this.lblIsFixedComparison.Text = "Is Fixed Comparison";
            this.lblIsFixedComparison.Visible = false;
            // 
            // chkIsFixedComparison
            // 
            this.chkIsFixedComparison.AutoSize = true;
            this.chkIsFixedComparison.Checked = true;
            this.chkIsFixedComparison.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsFixedComparison.Location = new System.Drawing.Point(170, 224);
            this.chkIsFixedComparison.Name = "chkIsFixedComparison";
            this.chkIsFixedComparison.Size = new System.Drawing.Size(15, 14);
            this.chkIsFixedComparison.TabIndex = 30;
            this.chkIsFixedComparison.UseVisualStyleBackColor = true;
            this.chkIsFixedComparison.Visible = false;
            this.chkIsFixedComparison.MouseCaptureChanged += new System.EventHandler(this.chkIsFixedComparison_MouseCaptureChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(170, 385);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 34;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Location = new System.Drawing.Point(662, 81);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(33, 13);
            this.lblClient.TabIndex = 40;
            this.lblClient.Text = "Client";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(522, 81);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(29, 13);
            this.lblYear.TabIndex = 39;
            this.lblYear.Text = "Year";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(128, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Study";
            // 
            // ddlClient
            // 
            this.ddlClient.FormattingEnabled = true;
            this.ddlClient.Location = new System.Drawing.Point(665, 97);
            this.ddlClient.Name = "ddlClient";
            this.ddlClient.Size = new System.Drawing.Size(121, 21);
            this.ddlClient.TabIndex = 37;
            this.ddlClient.Text = "Select a Client";
            this.ddlClient.SelectionChangeCommitted += new System.EventHandler(this.ddlClient_SelectionChangeCommitted);
            // 
            // ddlYear
            // 
            this.ddlYear.FormattingEnabled = true;
            this.ddlYear.Location = new System.Drawing.Point(525, 97);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(121, 21);
            this.ddlYear.TabIndex = 36;
            this.ddlYear.Text = "Select a Year";
            this.ddlYear.SelectionChangeCommitted += new System.EventHandler(this.ddlYear_SelectionChangeCommitted);
            // 
            // txtStudy
            // 
            this.txtStudy.Enabled = false;
            this.txtStudy.Location = new System.Drawing.Point(170, 6);
            this.txtStudy.Name = "txtStudy";
            this.txtStudy.Size = new System.Drawing.Size(171, 20);
            this.txtStudy.TabIndex = 41;
            // 
            // lblComparisonPercent
            // 
            this.lblComparisonPercent.AutoSize = true;
            this.lblComparisonPercent.Location = new System.Drawing.Point(60, 201);
            this.lblComparisonPercent.Name = "lblComparisonPercent";
            this.lblComparisonPercent.Size = new System.Drawing.Size(102, 13);
            this.lblComparisonPercent.TabIndex = 43;
            this.lblComparisonPercent.Text = "Comparison Percent";
            this.lblComparisonPercent.Visible = false;
            // 
            // txtComparisonPercent
            // 
            this.txtComparisonPercent.Location = new System.Drawing.Point(170, 198);
            this.txtComparisonPercent.Name = "txtComparisonPercent";
            this.txtComparisonPercent.Size = new System.Drawing.Size(100, 20);
            this.txtComparisonPercent.TabIndex = 42;
            this.txtComparisonPercent.Visible = false;
            // 
            // frmAddEditRuleDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 433);
            this.Controls.Add(this.lblComparisonPercent);
            this.Controls.Add(this.txtComparisonPercent);
            this.Controls.Add(this.txtStudy);
            this.Controls.Add(this.lblClient);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ddlClient);
            this.Controls.Add(this.ddlYear);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkIsFixedComparison);
            this.Controls.Add(this.lblIsFixedComparison);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtRuleDisplayName);
            this.Controls.Add(this.txtRuleName);
            this.Controls.Add(this.lblDataset);
            this.Controls.Add(this.lblColumn);
            this.Controls.Add(this.lblRow);
            this.Controls.Add(this.lblTable);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblComparisonValueDataType);
            this.Controls.Add(this.lblCustomValidationScript);
            this.Controls.Add(this.lblComparisonValue);
            this.Controls.Add(this.lblComparisonOperatorType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtValidationMsg);
            this.Controls.Add(this.txtCustomScript);
            this.Controls.Add(this.ddlDataSet);
            this.Controls.Add(this.ddlRow);
            this.Controls.Add(this.ddlColumn);
            this.Controls.Add(this.ddlTable);
            this.Controls.Add(this.ddlComparisonValueDataType);
            this.Controls.Add(this.txtComparisonValue);
            this.Controls.Add(this.ddlComparisonOperatorType);
            this.Controls.Add(this.txtOrder);
            this.Controls.Add(this.ddlConditionWithPrevReulStep);
            this.Controls.Add(this.ddlValidationType);
            this.Name = "frmAddEditRuleDetails";
            this.Text = "Add/Edit Rule Step";
            this.Load += new System.EventHandler(this.frmAddEditRuleDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlValidationType;
        private System.Windows.Forms.ComboBox ddlConditionWithPrevReulStep;
        private System.Windows.Forms.TextBox txtOrder;
        private System.Windows.Forms.ComboBox ddlComparisonOperatorType;
        private System.Windows.Forms.TextBox txtComparisonValue;
        private System.Windows.Forms.ComboBox ddlComparisonValueDataType;
        private System.Windows.Forms.ComboBox ddlTable;
        private System.Windows.Forms.ComboBox ddlColumn;
        private System.Windows.Forms.ComboBox ddlRow;
        private System.Windows.Forms.ComboBox ddlDataSet;
        private System.Windows.Forms.TextBox txtCustomScript;
        private System.Windows.Forms.TextBox txtValidationMsg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblComparisonOperatorType;
        private System.Windows.Forms.Label lblComparisonValue;
        private System.Windows.Forms.Label lblCustomValidationScript;
        private System.Windows.Forms.Label lblComparisonValueDataType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTable;
        private System.Windows.Forms.Label lblRow;
        private System.Windows.Forms.Label lblColumn;
        private System.Windows.Forms.Label lblDataset;
        private System.Windows.Forms.TextBox txtRuleName;
        private System.Windows.Forms.TextBox txtRuleDisplayName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblIsFixedComparison;
        private System.Windows.Forms.CheckBox chkIsFixedComparison;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ddlClient;
        private System.Windows.Forms.ComboBox ddlYear;
        private System.Windows.Forms.TextBox txtStudy;
        private System.Windows.Forms.Label lblComparisonPercent;
        private System.Windows.Forms.TextBox txtComparisonPercent;
    }
}