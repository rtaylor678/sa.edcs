﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Windows
{
    public class DataEvent
    {
        public static event EventHandler DataChanged;
        internal static void RaiseDataChanged(object sender, EventArgs e)
        {
            var handler = DataChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
        }
    }
}
