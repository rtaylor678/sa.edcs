﻿namespace Solomon.Edcs.Windows
{
    partial class frmDesignTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDesignTable));
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTableDisplayName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgColumn = new System.Windows.Forms.DataGridView();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.txtStudy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdateColumn = new System.Windows.Forms.Button();
            this.btnAddNewColumn = new System.Windows.Forms.Button();
            this.btnDeleteColumn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgColumn)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTableName
            // 
            this.txtTableName.Enabled = false;
            this.txtTableName.Location = new System.Drawing.Point(138, 44);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(243, 20);
            this.txtTableName.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Table Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(534, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "***";
            // 
            // txtTableDisplayName
            // 
            this.txtTableDisplayName.Enabled = false;
            this.txtTableDisplayName.Location = new System.Drawing.Point(138, 70);
            this.txtTableDisplayName.Name = "txtTableDisplayName";
            this.txtTableDisplayName.Size = new System.Drawing.Size(243, 20);
            this.txtTableDisplayName.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Table Display Name:";
            // 
            // dgColumn
            // 
            this.dgColumn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgColumn.Location = new System.Drawing.Point(138, 107);
            this.dgColumn.MultiSelect = false;
            this.dgColumn.Name = "dgColumn";
            this.dgColumn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgColumn.Size = new System.Drawing.Size(863, 357);
            this.dgColumn.TabIndex = 16;
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "DeleteRed.png");
            this.imgList.Images.SetKeyName(1, "Save.png");
            // 
            // txtStudy
            // 
            this.txtStudy.Enabled = false;
            this.txtStudy.Location = new System.Drawing.Point(466, 48);
            this.txtStudy.Name = "txtStudy";
            this.txtStudy.Size = new System.Drawing.Size(171, 20);
            this.txtStudy.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(427, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Study:";
            // 
            // btnUpdateColumn
            // 
            this.btnUpdateColumn.Location = new System.Drawing.Point(138, 486);
            this.btnUpdateColumn.Name = "btnUpdateColumn";
            this.btnUpdateColumn.Size = new System.Drawing.Size(139, 23);
            this.btnUpdateColumn.TabIndex = 19;
            this.btnUpdateColumn.Text = "Update Selected Column";
            this.btnUpdateColumn.UseVisualStyleBackColor = true;
            this.btnUpdateColumn.Click += new System.EventHandler(this.btnUpdateColumn_Click);
            // 
            // btnAddNewColumn
            // 
            this.btnAddNewColumn.Location = new System.Drawing.Point(298, 486);
            this.btnAddNewColumn.Name = "btnAddNewColumn";
            this.btnAddNewColumn.Size = new System.Drawing.Size(139, 23);
            this.btnAddNewColumn.TabIndex = 20;
            this.btnAddNewColumn.Text = "Add New Column";
            this.btnAddNewColumn.UseVisualStyleBackColor = true;
            this.btnAddNewColumn.Click += new System.EventHandler(this.btnAddNewColumn_Click);
            // 
            // btnDeleteColumn
            // 
            this.btnDeleteColumn.Location = new System.Drawing.Point(461, 486);
            this.btnDeleteColumn.Name = "btnDeleteColumn";
            this.btnDeleteColumn.Size = new System.Drawing.Size(139, 23);
            this.btnDeleteColumn.TabIndex = 21;
            this.btnDeleteColumn.Text = "Delete Column";
            this.btnDeleteColumn.UseVisualStyleBackColor = true;
            this.btnDeleteColumn.Click += new System.EventHandler(this.btnDeleteColumn_Click);
            // 
            // frmDesignTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 555);
            this.Controls.Add(this.btnDeleteColumn);
            this.Controls.Add(this.btnAddNewColumn);
            this.Controls.Add(this.btnUpdateColumn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtStudy);
            this.Controls.Add(this.dgColumn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTableDisplayName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTableName);
            this.Name = "frmDesignTable";
            this.Text = "Design Table";
            this.Load += new System.EventHandler(this.frmEditTable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgColumn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }        

        #endregion

        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTableDisplayName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgColumn;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.TextBox txtStudy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdateColumn;
        private System.Windows.Forms.Button btnAddNewColumn;
        private System.Windows.Forms.Button btnDeleteColumn;
    }
}