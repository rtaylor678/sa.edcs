﻿using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.Edcs.Utility;

namespace Solomon.Edcs.Windows
{
    public partial class frmRuleDetails : Form
    {
        public int selectedRuleId = 0;
        public frmRuleDetails()
        {
            InitializeComponent();            
        }

        private void BindDataSource()
        {
            BindingSource source = new BindingSource();

            source.DataSource = new AppRuleDetailService().GetAll(x => x.AppRuleId == selectedRuleId).ToList();

            dgRuleDetails.DataSource = source;
            dgRuleDetails.Columns[0].Visible = false;
        }

        private void frmRuleDetails_Load(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void btnEditRuleStep_Click(object sender, EventArgs e)
        {
            var frm = new frmAddEditRuleDetails();
            frm.selectedRuleId = dgRuleDetails.CurrentRow.Cells["AppRuleId"].Value.ToInt();
            frm.selectedRuleDetailId = dgRuleDetails.CurrentRow.Cells["AppRuleDetailId"].Value.ToInt();
            frm.Show();
        }

        private void btnAddNewRuleStep_Click(object sender, EventArgs e)
        {
            var frm = new frmAddEditRuleDetails();
            frm.selectedRuleId = selectedRuleId;
            frm.Show();
        }
    }
}
