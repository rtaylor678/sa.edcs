﻿using Solomon.Edcs.OrmTool;
using Solomon.Edcs.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Solomon.Edcs.Windows
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();            
        }

        private void ddlStudy_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateListBox();
        }
        
        private void ctxMnuListTables_MouseClick(object sender, MouseEventArgs e)
        {
            var selectedItem = ((ListBox)((ContextMenuStrip)sender).SourceControl).SelectedItem;
            var ctxMnu = sender as ContextMenuStrip;

            if (ctxMnu.Items[0].Selected)
            {
                var form = new frmAddTable();
                form.newTable = true;
                form.ShowDialog();                
                return;
            }

            if (selectedItem != null)
            {   
                if (ctxMnu.Items[1].Selected)
                {
                    var form = new frmAddTable();
                    form.newTable = false;
                    form.selectedTable = ((KeyValuePair<int, string>)selectedItem).Key;
                    form.ShowDialog();
                }
                if (ctxMnu.Items[2].Selected)
                {
                    var form = new frmDesignTable();
                    form.selectedTable = ((KeyValuePair<int, string>)selectedItem).Key;
                    form.study = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Value;
                    form.ShowDialog();
                }
                if (ctxMnu.Items[3].Selected)
                {
                    var form = new frmDesignTable();
                    form.selectedTable = ((KeyValuePair<int, string>)selectedItem).Key;
                    form.study = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Value;
                    form.IsDesignRows = true;
                    form.ShowDialog();
                }
                if (ctxMnu.Items[4].Selected)
                {
                    var form = new frmDataTable();
                    form.selectedTable = ((KeyValuePair<int, string>)selectedItem).Key;
                    form.ShowDialog();
                }
                if (ctxMnu.Items[5].Selected)
                {
                    var form = new frmValidationRule();
                    form.selectedTable = ((KeyValuePair<int, string>)selectedItem).Key;
                    form.table = ((KeyValuePair<int, string>)selectedItem).Value;
                    form.study = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Value;
                    form.selectedStudyId = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Key;
                    form.ShowDialog();
                }
            }
        }

        private void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateListBox();
        }

        private void ddlClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateListBox();
        }
        
        private void PopulateListBox()
        {
            Global.SelectedStudyId = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Key;
            Global.SelectedStudy = ((KeyValuePair<int, string>)ddlStudy.SelectedItem).Value;

            var service = new AppTableService();

           var tables = service.GetTableList(((KeyValuePair<int, string>)ddlStudy.SelectedItem).Key, ddlYear.SelectedItem != null ? ((KeyValuePair<int, string>)ddlYear.SelectedItem).Key : -1, ddlClient.SelectedItem != null ? ((KeyValuePair<int, string>)ddlClient.SelectedItem).Key : -1)
                .ToDictionary(x => x.AppTableId, x => x.Name + " - " + x.DisplayName);

            lstTables.DataSource = null;

            if (tables.Count > 0)
            {
                lstTables.DataSource = new BindingSource(tables, null);
                lstTables.DisplayMember = "Value";
                lstTables.ValueMember = "Key";
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void BindDataSource()
        {
            var service = new AppLookupService();

            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Study").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlStudy);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "StudyYear").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlYear);
            Utility.BindComboBox(service.GetAll(x => x.AppLookupType.Name == "Client").Select(x => new KeyValuePair<int, string>(x.Value, x.Name)).ToList(), ddlClient);
        }        
    }
}
