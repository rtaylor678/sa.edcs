﻿namespace Solomon.Edcs.Windows
{
    partial class frmRuleManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.dgRules = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRuleDetails = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgRules)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 44;
            this.label9.Text = "Validation Rules:";
            // 
            // dgRules
            // 
            this.dgRules.AllowUserToDeleteRows = false;
            this.dgRules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRules.Location = new System.Drawing.Point(42, 67);
            this.dgRules.MultiSelect = false;
            this.dgRules.Name = "dgRules";
            this.dgRules.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgRules.Size = new System.Drawing.Size(786, 295);
            this.dgRules.TabIndex = 43;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(42, 368);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 45;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRuleDetails
            // 
            this.btnRuleDetails.Location = new System.Drawing.Point(123, 367);
            this.btnRuleDetails.Name = "btnRuleDetails";
            this.btnRuleDetails.Size = new System.Drawing.Size(108, 23);
            this.btnRuleDetails.TabIndex = 46;
            this.btnRuleDetails.Text = "Rule Details";
            this.btnRuleDetails.UseVisualStyleBackColor = true;
            this.btnRuleDetails.Click += new System.EventHandler(this.btnRuleDetails_Click);
            // 
            // frmRuleManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 407);
            this.Controls.Add(this.btnRuleDetails);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dgRules);
            this.Name = "frmRuleManager";
            this.Text = "Rule Management";
            this.Load += new System.EventHandler(this.frmRuleManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRules)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgRules;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRuleDetails;
    }
}