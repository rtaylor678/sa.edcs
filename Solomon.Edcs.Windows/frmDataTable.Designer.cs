﻿namespace Solomon.Edcs.Windows
{
    partial class frmDataTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataTable));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlClient = new System.Windows.Forms.ComboBox();
            this.ddlYear = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblStudy = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlDataset = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.txtSubmissionId = new System.Windows.Forms.TextBox();
            this.txtCriteria = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCreateDataset = new System.Windows.Forms.Button();
            this.dgColumns = new System.Windows.Forms.DataGridView();
            this.ddlStudy = new System.Windows.Forms.ComboBox();
            this.ddlTable = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgColumns)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(233, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Client:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Year:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Study:";
            // 
            // ddlClient
            // 
            this.ddlClient.FormattingEnabled = true;
            this.ddlClient.Location = new System.Drawing.Point(271, 52);
            this.ddlClient.Name = "ddlClient";
            this.ddlClient.Size = new System.Drawing.Size(121, 21);
            this.ddlClient.TabIndex = 20;
            this.ddlClient.Text = "Select a Client";
            this.ddlClient.SelectionChangeCommitted += new System.EventHandler(this.ddlClient_SelectionChangeCommitted);
            // 
            // ddlYear
            // 
            this.ddlYear.FormattingEnabled = true;
            this.ddlYear.Location = new System.Drawing.Point(106, 49);
            this.ddlYear.Name = "ddlYear";
            this.ddlYear.Size = new System.Drawing.Size(121, 21);
            this.ddlYear.TabIndex = 19;
            this.ddlYear.Text = "Select a Year";
            this.ddlYear.SelectionChangeCommitted += new System.EventHandler(this.ddlYear_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(289, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Table:";
            // 
            // lblStudy
            // 
            this.lblStudy.AutoSize = true;
            this.lblStudy.Location = new System.Drawing.Point(328, 17);
            this.lblStudy.Name = "lblStudy";
            this.lblStudy.Size = new System.Drawing.Size(0, 13);
            this.lblStudy.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(402, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Submission Id:";
            // 
            // ddlDataset
            // 
            this.ddlDataset.Enabled = false;
            this.ddlDataset.FormattingEnabled = true;
            this.ddlDataset.Location = new System.Drawing.Point(106, 76);
            this.ddlDataset.Name = "ddlDataset";
            this.ddlDataset.Size = new System.Drawing.Size(58, 21);
            this.ddlDataset.TabIndex = 32;
            this.ddlDataset.SelectionChangeCommitted += new System.EventHandler(this.ddlDataset_SelectionChangeCommitted);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(40, 561);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 33;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "DeleteRed.png");
            this.imgList.Images.SetKeyName(1, "Save.png");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Select a Dataset:";
            // 
            // txtSubmissionId
            // 
            this.txtSubmissionId.Location = new System.Drawing.Point(476, 52);
            this.txtSubmissionId.Name = "txtSubmissionId";
            this.txtSubmissionId.Size = new System.Drawing.Size(38, 20);
            this.txtSubmissionId.TabIndex = 35;
            // 
            // txtCriteria
            // 
            this.txtCriteria.Location = new System.Drawing.Point(565, 53);
            this.txtCriteria.Name = "txtCriteria";
            this.txtCriteria.Size = new System.Drawing.Size(80, 20);
            this.txtCriteria.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(521, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Criteria:";
            // 
            // btnCreateDataset
            // 
            this.btnCreateDataset.Location = new System.Drawing.Point(658, 51);
            this.btnCreateDataset.Name = "btnCreateDataset";
            this.btnCreateDataset.Size = new System.Drawing.Size(86, 23);
            this.btnCreateDataset.TabIndex = 38;
            this.btnCreateDataset.Text = "Create Dataset";
            this.btnCreateDataset.UseVisualStyleBackColor = true;
            this.btnCreateDataset.Click += new System.EventHandler(this.btnCreateDataset_Click);
            // 
            // dgColumns
            // 
            this.dgColumns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgColumns.Location = new System.Drawing.Point(40, 114);
            this.dgColumns.Name = "dgColumns";
            this.dgColumns.Size = new System.Drawing.Size(885, 441);
            this.dgColumns.TabIndex = 39;
            // 
            // ddlStudy
            // 
            this.ddlStudy.FormattingEnabled = true;
            this.ddlStudy.Location = new System.Drawing.Point(109, 14);
            this.ddlStudy.Name = "ddlStudy";
            this.ddlStudy.Size = new System.Drawing.Size(121, 21);
            this.ddlStudy.TabIndex = 40;
            this.ddlStudy.Text = "Select a Study";
            this.ddlStudy.SelectionChangeCommitted += new System.EventHandler(this.ddlStudy_SelectionChangeCommitted);
            // 
            // ddlTable
            // 
            this.ddlTable.FormattingEnabled = true;
            this.ddlTable.Location = new System.Drawing.Point(334, 13);
            this.ddlTable.Name = "ddlTable";
            this.ddlTable.Size = new System.Drawing.Size(311, 21);
            this.ddlTable.TabIndex = 41;
            this.ddlTable.Text = "Select a Table";
            this.ddlTable.SelectionChangeCommitted += new System.EventHandler(this.ddlTable_SelectionChangeCommitted);
            // 
            // frmDataTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 627);
            this.Controls.Add(this.ddlTable);
            this.Controls.Add(this.ddlStudy);
            this.Controls.Add(this.dgColumns);
            this.Controls.Add(this.btnCreateDataset);
            this.Controls.Add(this.txtCriteria);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtSubmissionId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.ddlDataset);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblStudy);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlClient);
            this.Controls.Add(this.ddlYear);
            this.Name = "frmDataTable";
            this.Text = "Data Table";
            this.Load += new System.EventHandler(this.frmDataTable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgColumns)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgDataTable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlClient;
        private System.Windows.Forms.ComboBox ddlYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblStudy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddlDataset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSubmissionId;
        private System.Windows.Forms.TextBox txtCriteria;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCreateDataset;
        private System.Windows.Forms.DataGridView dgColumns;
        private System.Windows.Forms.ComboBox ddlStudy;
        private System.Windows.Forms.ComboBox ddlTable;
    }
}