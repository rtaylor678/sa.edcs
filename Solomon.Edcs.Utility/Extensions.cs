﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Solomon.Edcs.Utility
{
    public static class Extensions
    {
        /// <summary>
        /// Converts an enum to is description attribute if it has one or the value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToString(this Enum value)
        {
            if (value == null)
                throw new ArgumentNullException("value");
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(Enum.GetName(type, value));
            var descriptionAttribute =
                (DescriptionAttribute)Attribute.GetCustomAttribute(
                                            fieldInfo, typeof(DescriptionAttribute));

            if (descriptionAttribute != null)
                return descriptionAttribute.Description;
            return value.ToString();
        }

        public static IDictionary GetValues<T>(this T value, Type type)
        {
            return GetValues<T>(value, type, new List<string>());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static IDictionary GetValues<T>(this T value, Type type, List<string> filter)
        {
            IDictionary list = new Dictionary<string, object>();
            var properties = typeof(T)
                                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                .Where(ø => ø.CanRead && ø.CanWrite)
                                .Where(ø => ø.PropertyType == type)
                                .Where(ø => ø.GetGetMethod(true).IsPublic)
                                .Where(ø => ø.GetSetMethod(true).IsPublic)
                                .Where(ø => !filter.Contains(ø.Name));

            foreach (PropertyInfo p in properties)
                list.Add(p.Name, p.GetValue(value, null));
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static object GetValue<T>(this T value, string name)
        {
            object prop = null;
            var p = typeof(T).GetProperty(name);
            if (p != null)
                prop = p.GetValue(value, null);
            return prop;
        }

        public static void SetValue<T>(this T obj, string name, object value)
        {
            var p = typeof(T).GetProperty(name);
            if (p != null)
                p.SetValue(obj, value, null);
        }
        public static string TimeSpanToString(this TimeSpan? value)
        {
            if (value.HasValue)
            {
                return value.Value.Hours.ToString().PadLeft(2, '0') + ":" +
                    value.Value.Minutes.ToString().PadLeft(2, '0');
            }
            return string.Empty;
        }

        public static bool HasAlpaNumericChars(this string value)
        {
            if (string.IsNullOrEmpty(value)) return false;
            char[] arr = value.ToCharArray();
            arr = Array.FindAll<char>(arr, (c => (char.IsLetterOrDigit(c))));
            return arr.Length > 0;
        }
        public static bool HasNonAlpaNumericChars(this string value)
        {
            if (string.IsNullOrEmpty(value)) return false;
            char[] arr = value.ToCharArray();
            arr = Array.FindAll<char>(arr, (c => (!char.IsLetterOrDigit(c))));
            return arr.Length > 0;
        }
        public static bool HasNumberChars(this string value)
        {
            if (string.IsNullOrEmpty(value)) return false;
            char[] arr = value.ToCharArray();
            arr = Array.FindAll<char>(arr, (c => (char.IsDigit(c))));
            return arr.Length > 0;
        }
        public static bool HasNumberChars(this string value, int minToHave)
        {
            if (string.IsNullOrEmpty(value)) return false;
            char[] arr = value.ToCharArray();
            arr = Array.FindAll<char>(arr, (c => (char.IsDigit(c))));
            return arr.Length >= minToHave;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveSpecialCharacters(this string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsValidEmail(this string value)
        {
            bool invalid = false;
            if (String.IsNullOrEmpty(value))
                return false;
            try
            {
                // Use IdnMapping class to convert Unicode domain names.
                value = Regex.Replace(value, @"(@)(.+)$", DomainMapper);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(value,
                   @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                   RegexOptions.IgnoreCase);
        }

        private static string DomainMapper(this Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();
            string domainName = match.Groups[2].Value;
            domainName = idn.GetAscii(domainName);
            return match.Groups[1].Value + domainName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeToSubtractFrom"></param>
        /// <param name="timeToBeSubtracted"></param>
        /// <returns></returns>
        public static double SubtractTime(this TimeSpan timeToSubtractFrom, TimeSpan timeToBeSubtracted)
        {
            double time = 0;
            TimeSpan tspan = timeToSubtractFrom.Subtract(timeToBeSubtracted);
            int hrs = tspan.Hours + (tspan.Days * 24);
            double.TryParse(string.Format("{0}.{1}", hrs, tspan.Minutes.ToString().PadLeft(2, '0').PadRight(2, '0')), out time);
            return time;
        }

        public static decimal ToDecimal(this TimeSpan? timeSpan)
        {
            decimal retVal = 0;

            if (timeSpan.HasValue)
            {
                decimal.TryParse(string.Format("{0}.{1}", timeSpan.Value.Hours, timeSpan.Value.Minutes.ToString().PadLeft(2, '0').PadRight(2, '0')), out retVal);
            }

            return retVal;
        }
        public static decimal ToDecimal(this TimeSpan timeSpan)
        {
            decimal retVal = 0;

            decimal.TryParse(string.Format("{0}.{1}", timeSpan.Hours, timeSpan.Minutes.ToString().PadLeft(2, '0').PadRight(2, '0')), out retVal);

            return retVal;
        }

        public static int ToInt(this string stringValue)
        {
            int retVal = 0;
            if (!string.IsNullOrWhiteSpace(stringValue))
            {
                int.TryParse(stringValue, out retVal);
            }

            return retVal;
        }

        public static int ToInt(this object objValue)
        {
            int retVal = 0;
            if (objValue != null)
            {
                int.TryParse(objValue.ToString(), out retVal);
            }

            return retVal;
        }

        public static int? ToNullableInt(this string stringValue)
        {            
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return null;
            }
            else
            {
                int retVal = 0;
                int.TryParse(stringValue, out retVal);

                return retVal;
            }
        }

        public static int? ToNullableInt(this object objValue)
        {
            if (objValue == null)
            {
                return null;
            }
            else
            {
                int retVal = 0;
                int.TryParse(objValue.ToString(), out retVal);

                return retVal;
            }
        }

        //-----
        public static bool ToBool(this string stringValue)
        {
            bool retVal = false;
            if (!string.IsNullOrWhiteSpace(stringValue))
            {
                bool.TryParse(stringValue, out retVal);
            }

            return retVal;
        }

        public static bool ToBool(this object objValue)
        {
            bool retVal = false;
            if (objValue != null)
            {
                bool.TryParse(objValue.ToString(), out retVal);
            }

            return retVal;
        }

        public static bool? ToNullableBool(this string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return null;
            }
            else
            {
                bool retVal = false;
                bool.TryParse(stringValue, out retVal);

                return retVal;
            }
        }

        public static bool? ToNullableBool(this object objValue)
        {
            if (objValue == null)
            {
                return null;
            }
            else
            {
                bool retVal = false;
                bool.TryParse(objValue.ToString(), out retVal);

                return retVal;
            }
        }
        //----
        public static DateTime ToDateTime(this string stringValue)
        {
            DateTime retVal = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(stringValue))
            {
                DateTime.TryParse(stringValue, out retVal);
            }

            return retVal;
        }

        public static DateTime ToDateTime(this object objValue)
        {
            DateTime retVal = DateTime.MinValue;
            if (objValue != null)
            {
                DateTime.TryParse(objValue.ToString(), out retVal);
            }

            return retVal;
        }

        public static DateTime? ToNullableDateTime(this string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return null;
            }
            else
            {
                DateTime retVal = DateTime.MinValue;
                DateTime.TryParse(stringValue, out retVal);

                return retVal;
            }
        }

        public static DateTime? ToNullableDateTime(this object objValue)
        {
            if (objValue == null)
            {
                return null;
            }
            else
            {
                DateTime retVal = DateTime.MinValue;
                DateTime.TryParse(objValue.ToString(), out retVal);

                return retVal;
            }
        }
        public static decimal ToDecimal(this string stringValue)
        {
            decimal retVal = 0m;
            if (!string.IsNullOrWhiteSpace(stringValue))
            {
                decimal.TryParse(stringValue, out retVal);
            }

            return retVal;
        }

        public static decimal ToDecimal(this object objValue)
        {
            decimal retVal = 0m;
            if (objValue != null)
            {
                decimal.TryParse(objValue.ToString(), out retVal);
            }

            return retVal;
        }

        public static decimal? ToNullableDecimal(this string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return null;
            }
            else
            {
                decimal retVal = 0m;
                decimal.TryParse(stringValue, out retVal);

                return retVal;
            }
        }
        public static decimal? ToNullableDecimal(this object objValue)
        {
            if (objValue == null)
            {
                return null;
            }
            else
            {
                decimal retVal = 0m;
                decimal.TryParse(objValue.ToString(), out retVal);

                return retVal;
            }
        }
    }
}
