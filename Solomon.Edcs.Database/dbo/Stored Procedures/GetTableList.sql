﻿-- =============================================
-- Author:		Omar Azad
-- Create date: 04/04/2016
-- DescriptiON:	Returns Table Data BY Table Name and optiONal criteria
-- =============================================
CREATE PROCEDURE [dbo].[GetTableList]
@Study INT,
@Year INT = 0,
@ClientId INT = 0

AS

--DECLARE 
--@Study INT,
--@Year INT = 0,
--@ClientId INT = 0

--SET @Study = 1
--SET @Year = 2016
--SET @ClientId = 1

BEGIN
	SET NOCOUNT ON;
	--SELECT * FROM [dbo].[AppTable] 
	SELECT * FROM [dbo].[AppTable] WHERE Study = @Study AND ([Year] IS NULL OR [Year] = @Year) AND ([Client] IS NULL OR [Client] = @ClientId)
	
END
