﻿-- =============================================
-- Author:		Omar Azad
-- Create date: 05/10/2016
-- Description:	Returns Table Data BY Table Name and optional criteria
-- =============================================
CREATE PROCEDURE [dbo].[ValidateTableData]
@TableData TableDataType READONLY,
@ErrorMessage VARCHAR(MAX) OUTPUT
AS

BEGIN	
	SET NOCOUNT ON;

----Test Script-------------------------------------------------------------------
--declare @TableData as [dbo].[TableDataType], @ErrorMessage varchar(max)

--insert into @TableData
--values(1, 1, 1, 1, '', 567, 0, null)

----End Test Script-------------------------------------------------------------------


DECLARE @Count INT





SET @COUNT = (SELECT COUNT(ID) FROM @TableData)

WHILE @Count > 0
BEGIN

	



	PRINT @COUNT

	SET @Count = @Count - 1

END


SELECT * FROM [dbo].[AppDataSet]  


	SET @ErrorMessage = 'Error occured'
	
	--SELECT * FROM @TableData
    --@ErrorMessage
END