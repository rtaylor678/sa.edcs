﻿-- =============================================
-- Author:		Omar Azad
-- Create date: 03/30/2016
-- DescriptiON:	Returns Table Data BY Table Name and optional criteria
-- =============================================
CREATE PROCEDURE [dbo].[GetTableData]
@TableName VARCHAR(250),
@Study VARCHAR(250),
@Year INT = 0,
@ClientId INT = 0

AS
BEGIN	
	SET NOCOUNT ON;

	DECLARE 
	--@TableName VARCHAR(50), 
	@TableId INT, 
	@Query NVARCHAR(MAX), @cols AS VARCHAR(MAX), @Counter INT

	SET @TableId = 0
	SET @Counter = 0
	--SET @TableName = 'CrudeChargeDetail'
	SET @TableId = (SELECT TOP 1 AppTableId FROM AppTable WHERE Name ='' + @TableName + '' AND Study = '' + @Study + '' )

	EXEC [dbo].[GetTableDataById] @TableId, @Year, @ClientId
    
END
