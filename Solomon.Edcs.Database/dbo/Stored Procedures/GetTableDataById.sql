﻿
 --=============================================
 --Author:		Omar Azad
 --Create date: 04/14/2016
 --DescriptiON:	Returns Table Data BY Table Name and optional criteria
 --=============================================
CREATE PROCEDURE [dbo].[GetTableDataById]
(
	@TableId INT,
    @AppDataSetId INT,
	@Year INT = 0,
	@ClientId INT = 0
)
AS


BEGIN	
	
	SET NOCOUNT ON;

--DECLARE	
--	@TableId INT,
--	@Year INT,
--	@ClientId INT,
--	@AppDataSetId INT

--	SET @TableId = 1
--	SET @Year = 0
--	SET @ClientId = 0
--	SET @AppDataSetId = 1
	
	DECLARE @Query NVARCHAR(MAX), @cols AS VARCHAR(MAX), @Counter INT


	SET @Counter = 0





	DECLARE @ColumnName VARCHAR(250), @DataTypeId INT

	--Create Output Data Table
	--**********************************************************************************************
	SET @Query = (SELECT [dbo].[fnCreateOutputTable] (@TableId, @Year, @ClientId))
	--PRINT @Query

	--**************************************Decimal Table*****************************************
	--First Table just insert
	--********************************************************************************************

	SELECT @cols = (SELECT [dbo].[fnGetTableColumns] (@TableId, 2, @Year, @ClientId))


	SET @Query = @Query + N' INSERT INTO @DataTable
	(AppRowId, ' + @cols + ' )'


	SET @Query = @Query + N' SELECT AppRowId, ' + @cols + ' FROM 
				(
					SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueDecimal] d
					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId 
					WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + '
			   ) x
				pivot 
				(
					MAX(CellValue)
					FOR ColumnName in ( ' + @cols + ' )
				) p '

--PRINT @Query

	--**************************************END Decimal Table*************************************
	--********************************************************************************************


	--*************************BEGIN Text table **********************************************
	--********************************************************************************************

	DECLARE @ColumnTable TABLE (Id INT IDENTITY(1,1), StringColumn VARCHAR(250))

	SELECT @cols = (SELECT [dbo].[fnGetTableColumns] (@TableId, 1, @Year, @ClientId))


	INSERT INTO @ColumnTable
	(StringColumn)
	SELECT splitdata FROM [dbo].[fnSplitString](@cols, ',')


	SET @Query = @Query + N' UPDATE t2'

	SET @Counter = 0
	DECLARE rowCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT Id, StringColumn FROM @ColumnTable
	OPEN rowCursor
	FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF @Counter = 0
			BEGIN
				SET @Query = @Query + ' SET t2.' + @ColumnName + ' = t1.' + @ColumnName
			END

		ELSE
			BEGIN
				SET @Query = @Query + ', t2.' + @ColumnName + ' = t1.' + @ColumnName
			END
		SET @Counter = 1
		FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName
	END

	CLOSE rowCursor
	DEALLOCATE rowCursor


	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' (SELECT AppRowId,' + @cols + ' FROM '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueText] d'
	SET @Query = @Query + N' INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId '
	SET @Query = @Query + N' WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + ''
	SET @Query = @Query + N' ) x'
	SET @Query = @Query + N' pivot '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' MAX(CellValue)'
	SET @Query = @Query + N' FOR ColumnName in (' + @cols + ' )'
	SET @Query = @Query + N' ) p) t1 INNER JOIN @DataTable as t2 ON t1.AppRowId = t2.AppRowId  '

	SET @Query = @Query + N' INSERT INTO @DataTable ' 
	SET @Query = @Query + N' (AppRowId, ' + @cols + ' )'
	SET @Query = @Query + N' SELECT  t2.* '
	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' 	( '
	SET @Query = @Query + N' 		SELECT AppRowId, ' + @cols + ' '
	SET @Query = @Query + N' 		FROM	(  '
	SET @Query = @Query + N' 					SELECT d.AppRowId, d.CellValue,c.Name AS ColumnName '
	SET @Query = @Query + N' 					FROM [dbo].[AppCellValueText] d '
	SET @Query = @Query + N' 					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId ' 
	SET @Query = @Query + N' WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + ''
	SET @Query = @Query + N' 				) x '
	SET @Query = @Query + N' 				pivot '  
	SET @Query = @Query + N' 				( '
	SET @Query = @Query + N' 					MAX(CellValue) '
	SET @Query = @Query + N' 					FOR ColumnName in (' + @cols + ' ) '
	SET @Query = @Query + N' 				) p '
	SET @Query = @Query + N' 	) t2 '
	SET @Query = @Query + N' 	LEFT JOIN @DataTable AS t1 ON t2.AppRowId = t1.AppRowId '
	SET @Query = @Query + N' WHERE t1.AppRowId IS NULL '

	--*************************END Text table **********************************************
	--********************************************************************************************


	--*************************BEGIN INT table **********************************************
	--********************************************************************************************

	SELECT @cols = (SELECT [dbo].[fnGetTableColumns] (@TableId, 3, @Year, @ClientId))

	DELETE FROM @ColumnTable

	SET @Counter = 0
	INSERT INTO @ColumnTable
	(StringColumn)
	SELECT splitdata FROM [dbo].[fnSplitString](@cols, ',')


	SET @Query = @Query + N' UPDATE t2'

	DECLARE rowCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT Id, StringColumn FROM @ColumnTable
	OPEN rowCursor
	FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @Counter = 0
			BEGIN
				SET @Query = @Query + ' SET t2.' + @ColumnName + ' = t1.' + @ColumnName
			END

		ELSE
			BEGIN
				SET @Query = @Query + ', t2.' + @ColumnName + ' = t1.' + @ColumnName
			END
		SET @Counter = 1
		FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName
	END

	CLOSE rowCursor
	DEALLOCATE rowCursor


	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' (SELECT AppRowId,' + @cols + ' FROM '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueINT] d'
	SET @Query = @Query + N' INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId '
	SET @Query = @Query + N' WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + ''
	SET @Query = @Query + N' ) x'
	SET @Query = @Query + N' pivot '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' MAX(CellValue)'
	SET @Query = @Query + N' FOR ColumnName in (' + @cols + ' )'
	SET @Query = @Query + N' ) p) t1 INNER JOIN @DataTable as t2 ON t1.AppRowId = t2.AppRowId  '

	SET @Query = @Query + N' INSERT INTO @DataTable ' 
	SET @Query = @Query + N' (AppRowId, ' + @cols + ' )'
	SET @Query = @Query + N' SELECT  t2.* '
	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' 	( '
	SET @Query = @Query + N' 		SELECT AppRowId,' + @cols + ' '
	SET @Query = @Query + N' 		FROM	(  '
	SET @Query = @Query + N' 					SELECT d.AppRowId, d.CellValue,c.Name AS ColumnName '
	SET @Query = @Query + N' 					FROM [dbo].[AppCellValueINT] d '
	SET @Query = @Query + N' 					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId ' 
	SET @Query = @Query + N' WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + ''
	SET @Query = @Query + N' 				) x '
	SET @Query = @Query + N' 				pivot '  
	SET @Query = @Query + N' 				( '
	SET @Query = @Query + N' 					MAX(CellValue) '
	SET @Query = @Query + N' 					FOR ColumnName in (' + @cols + ' ) '
	SET @Query = @Query + N' 				) p '
	SET @Query = @Query + N' 	) t2 '
	SET @Query = @Query + N' 	LEFT JOIN @DataTable AS t1 ON t2.AppRowId = t1.AppRowId '
	SET @Query = @Query + N' WHERE t1.AppRowId IS NULL '

	--*************************END INT table **********************************************
	--********************************************************************************************

	
	--*************************BEGIN DateTime table **********************************************
	--********************************************************************************************

	SELECT @cols = (SELECT [dbo].[fnGetTableColumns] (@TableId, 4, @Year, @ClientId))
	
	--PRINT @cols

	DELETE FROM @ColumnTable

	SET @Counter = 0
	INSERT INTO @ColumnTable
	(StringColumn)
	SELECT splitdata FROM [dbo].[fnSplitString](@cols, ',')


	SET @Query = @Query + N' UPDATE t2'

	DECLARE rowCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT Id, StringColumn FROM @ColumnTable
	OPEN rowCursor
	FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @Counter = 0
			BEGIN
				SET @Query = @Query + ' SET t2.' + @ColumnName + ' = t1.' + @ColumnName
			END

		ELSE
			BEGIN
				SET @Query = @Query + ', t2.' + @ColumnName + ' = t1.' + @ColumnName
			END
		SET @Counter = 1
		FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName
	END

	CLOSE rowCursor
	DEALLOCATE rowCursor


	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' (SELECT AppRowId,' + @cols + ' FROM '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueDateTime] d'
	SET @Query = @Query + N' INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId '
	SET @Query = @Query + N' WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + ''
	SET @Query = @Query + N' ) x'
	SET @Query = @Query + N' pivot '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' MAX(CellValue)'
	SET @Query = @Query + N' FOR ColumnName in (' + @cols + ' )'
	SET @Query = @Query + N' ) p) t1 INNER JOIN @DataTable as t2 ON t1.AppRowId = t2.AppRowId  '

	SET @Query = @Query + N' INSERT INTO @DataTable ' 
	SET @Query = @Query + N' (AppRowId, ' + @cols + ' )'
	SET @Query = @Query + N' SELECT  t2.* '
	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' 	( '
	SET @Query = @Query + N' 		SELECT AppRowId,' + @cols + ' '
	SET @Query = @Query + N' 		FROM	(  '
	SET @Query = @Query + N' 					SELECT d.AppRowId, d.CellValue,c.Name AS ColumnName '
	SET @Query = @Query + N' 					FROM [dbo].[AppCellValueDateTime] d '
	SET @Query = @Query + N' 					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId ' 
	SET @Query = @Query + N' WHERE d.AppDataSetId = ' + CONVERT(VARCHAR(15), @AppDataSetId) + ''
	SET @Query = @Query + N' 				) x '
	SET @Query = @Query + N' 				pivot '  
	SET @Query = @Query + N' 				( '
	SET @Query = @Query + N' 					MAX(CellValue) '
	SET @Query = @Query + N' 					FOR ColumnName in (' + @cols + ' ) '
	SET @Query = @Query + N' 				) p '
	SET @Query = @Query + N' 	) t2 '
	SET @Query = @Query + N' 	LEFT JOIN @DataTable AS t1 ON t2.AppRowId = t1.AppRowId '
	SET @Query = @Query + N' WHERE t1.AppRowId IS NULL '

	--*************************END DateTime table **********************************************
	--********************************************************************************************

	SET @Query = @Query + 'SELECT * FROM @DataTable'			

	--PRINT @Query
	EXEC (@Query)
    
END