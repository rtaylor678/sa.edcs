﻿-- =============================================
-- Author:		Omar Azad
-- Create date: 04/18/2016
-- DescriptiON:	Returns Table Data in combined cell value by TableId and optional criteria
-- =============================================
CREATE PROCEDURE [dbo].[GetTableDataCellValue]
@TableId INT,
@Year INT = 0,
@ClientId INT = 0,
@DataSetId INT = 0

AS

BEGIN	
	SET NOCOUNT ON;


-------------------------------------------------------------------------------------------------------------------------
----For Testing
--	declare
--	@TableId INT,
--	@DataSetId INT,
--	@Year INT, 
--	@ClientId INT 

--	SET @TableId = 4
--	SET @DataSetId = 0
--	SET @Year = 0
--	SET @ClientId = 0
----End Test script
-------------------------------------------------------------------------------------------------------------------------

declare @DataTable TABLE(RowId INT, CellValue NVARCHAR(MAX), ColumnName varchar(250), ColumnId INT, AppDataSetId INT)

-------------------------------------------------------------------------------------------------------------------------
--Data From Row Table
INSERT INTO @DataTable
SELECT r.AppRowId, r.Name, c.Name, c.AppColumnId, @DataSetId FROM [dbo].[AppRow] r
INNER JOIN [dbo].[AppColumn] c on r.[AssociateColumnId] = c.AppColumnId
WHERE r.AppTableId = @TableId AND (r.[Year] IS NULL OR r.[Year] = @Year)  AND (r.ClientId IS NULL OR r.ClientId = @ClientId)
	
-------------------------------------------------------------------------------------------------------------------------	

-------------------------------------------------------------------------------------------------------------------------	
	INSERT INTO @DataTable
	SELECT d.AppRowId, d.CellValue,c.Name as ColumnName, c.AppColumnId, d.AppDataSetId  FROM [dbo].[AppCellValueText] d
		INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId 
		WHERE c.AppTableId = @TableId AND (@DataSetId < 1 OR d.AppDataSetId = @DataSetId) AND ([Year] IS NULL OR [Year] = @Year)  AND (ClientId IS NULL OR ClientId = @ClientId)
	
	INSERT INTO @DataTable
	SELECT d.AppRowId, d.CellValue,c.Name as ColumnName, c.AppColumnId, d.AppDataSetId  FROM [dbo].[AppCellValueInt] d
		INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId 
		WHERE c.AppTableId = @TableId AND (@DataSetId < 1 OR d.AppDataSetId = @DataSetId)  AND ([Year] IS NULL OR [Year] = @Year)  AND (ClientId IS NULL OR ClientId = @ClientId)
	
	INSERT INTO @DataTable
	SELECT d.AppRowId, d.CellValue,c.Name as ColumnName, c.AppColumnId, d.AppDataSetId  FROM [dbo].AppCellValueDateTime d
		INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId 
		WHERE c.AppTableId = @TableId AND (@DataSetId < 1 OR d.AppDataSetId = @DataSetId)  AND ([Year] IS NULL OR [Year] = @Year)  AND (ClientId IS NULL OR ClientId = @ClientId)
	
	INSERT INTO @DataTable
	SELECT d.AppRowId, d.CellValue,c.Name as ColumnName, c.AppColumnId, d.AppDataSetId  FROM [dbo].AppCellValueDecimal d
	INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId 
	WHERE c.AppTableId = @TableId AND (@DataSetId < 1 OR d.AppDataSetId = @DataSetId)  AND ([Year] IS NULL OR [Year] = @Year)  AND (ClientId IS NULL OR ClientId = @ClientId)

	SELECT * FROM @DataTable ORDER BY AppDataSetId, RowId, ColumnId
-------------------------------------------------------------------------------------------------------------------------
    
END