﻿
-- =============================================
-- Author:		Omar Azad
-- Create date: 04/14/2016
-- DescriptiON:	Returns Table Data BY Table Name and optional criteria
-- =============================================
CREATE PROCEDURE [dbo].[GetTableDataById_Test]
(
	@TableId INT,
	@Year INT = 0,
	@ClientId INT = 0
)
AS
BEGIN	
	
	SET NOCOUNT ON;

	DECLARE 
	--@TableName VARCHAR(50), 
	--@TableId INT, 
	@Query NVARCHAR(MAX), @cols AS VARCHAR(MAX), @Counter INT


	SET @Counter = 0
	--SET @TableName = 'CrudeChargeDetail'
	--SET @TableId = (SELECT TOP 1 AppTableId FROM AppTable WHERE Name ='' + @TableName + '' )





	DECLARE @ColumnName VARCHAR(250), @DataTypeId INT

	--Create Output Data Table
	--**********************************************************************************************
	SET @Query = (SELECT [dbo].[fnCreateOutputTable] (@TableId, @Year, @ClientId))
	--PRINT @Query

	--**************************************Decimal Table*****************************************
	--First Table just insert
	--********************************************************************************************

	SELECT @cols = (SELECT [dbo].[fnGetTableColumns] (@TableId, 2, @Year, @ClientId))


	SET @Query = @Query + N' INSERT INTO @DataTable
	(AppRowId, ' + @cols + ' )'


	SET @Query = @Query + N' SELECT AppRowId, ' + @cols + ' FROM 
				(
					SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueDecimal] d
					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId 
					WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')
			   ) x
				pivot 
				(
					MAX(CellValue)
					FOR ColumnName in ( ' + @cols + ' )
				) p '

PRINT @Query

	--**************************************END Decimal Table*************************************
	--********************************************************************************************


	--*************************BEGIN Text table **********************************************
	--********************************************************************************************

	DECLARE @ColumnTable TABLE (Id INT IDENTITY(1,1), StringColumn VARCHAR(250))

	SELECT @cols = STUFF((SELECT ',' + QUOTENAME(Name) 
						FROM [dbo].[AppColumn] WHERE AppTableId = @TableId AND ([Year] IS NULL OR [Year] = @Year) AND (ClientId IS NULL OR ClientId = @ClientId) and DataTypeId = 1
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'')


	INSERT INTO @ColumnTable
	(StringColumn)
	SELECT splitdata FROM [dbo].[fnSplitString](@cols, ',')


	SET @Query = @Query + N' UPDATE t2'

	SET @Counter = 0
	DECLARE rowCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT Id, StringColumn FROM @ColumnTable
	OPEN rowCursor
	FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName 
	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF @Counter = 0
			BEGIN
				SET @Query = @Query + ' SET t2.' + @ColumnName + ' = t1.' + @ColumnName
			END

		ELSE
			BEGIN
				SET @Query = @Query + ', t2.' + @ColumnName + ' = t1.' + @ColumnName
			END
		SET @Counter = 1
		FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName
	END

	CLOSE rowCursor
	DEALLOCATE rowCursor


	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' (SELECT AppRowId,' + @cols + ' FROM '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueText] d'
	SET @Query = @Query + N' INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId '
	SET @Query = @Query + N' WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')'
	SET @Query = @Query + N' ) x'
	SET @Query = @Query + N' pivot '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' MAX(CellValue)'
	SET @Query = @Query + N' FOR ColumnName in (' + @cols + ' )'
	SET @Query = @Query + N' ) p) t1 INNER JOIN @DataTable as t2 ON t1.AppRowId = t2.AppRowId  '

	SET @Query = @Query + N' INSERT INTO @DataTable ' 
	SET @Query = @Query + N' (AppRowId, ' + @cols + ' )'
	SET @Query = @Query + N' SELECT  t2.* '
	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' 	( '
	SET @Query = @Query + N' 		SELECT AppRowId, ' + @cols + ' '
	SET @Query = @Query + N' 		FROM	(  '
	SET @Query = @Query + N' 					SELECT d.AppRowId, d.CellValue,c.Name AS ColumnName '
	SET @Query = @Query + N' 					FROM [dbo].[AppCellValueText] d '
	SET @Query = @Query + N' 					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId ' 
	SET @Query = @Query + N' WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')'
	SET @Query = @Query + N' 				) x '
	SET @Query = @Query + N' 				pivot '  
	SET @Query = @Query + N' 				( '
	SET @Query = @Query + N' 					MAX(CellValue) '
	SET @Query = @Query + N' 					FOR ColumnName in (' + @cols + ' ) '
	SET @Query = @Query + N' 				) p '
	SET @Query = @Query + N' 	) t2 '
	SET @Query = @Query + N' 	LEFT JOIN @DataTable AS t1 ON t2.AppRowId = t1.AppRowId '
	SET @Query = @Query + N' WHERE t1.AppRowId IS NULL '

	--*************************END Text table **********************************************
	--********************************************************************************************


	--*************************BEGIN INT table **********************************************
	--********************************************************************************************

	SELECT @cols = STUFF((SELECT ',' + QUOTENAME(Name) 
						FROM [dbo].[AppColumn] WHERE AppTableId = @TableId AND ([Year] IS NULL OR [Year] = @Year) AND (ClientId IS NULL OR ClientId = @ClientId) and DataTypeId = 3
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'')

	DELETE FROM @ColumnTable

	SET @Counter = 0
	INSERT INTO @ColumnTable
	(StringColumn)
	SELECT splitdata FROM [dbo].[fnSplitString](@cols, ',')


	SET @Query = @Query + N' UPDATE t2'

	DECLARE rowCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT Id, StringColumn FROM @ColumnTable
	OPEN rowCursor
	FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @Counter = 0
			BEGIN
				SET @Query = @Query + ' SET t2.' + @ColumnName + ' = t1.' + @ColumnName
			END

		ELSE
			BEGIN
				SET @Query = @Query + ', t2.' + @ColumnName + ' = t1.' + @ColumnName
			END
		SET @Counter = 1
		FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName
	END

	CLOSE rowCursor
	DEALLOCATE rowCursor


	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' (SELECT AppRowId,' + @cols + ' FROM '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueINT] d'
	SET @Query = @Query + N' INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId '
	SET @Query = @Query + N' WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')'
	SET @Query = @Query + N' ) x'
	SET @Query = @Query + N' pivot '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' MAX(CellValue)'
	SET @Query = @Query + N' FOR ColumnName in (' + @cols + ' )'
	SET @Query = @Query + N' ) p) t1 INNER JOIN @DataTable as t2 ON t1.AppRowId = t2.AppRowId  '

	SET @Query = @Query + N' INSERT INTO @DataTable ' 
	SET @Query = @Query + N' (AppRowId, ' + @cols + ' )'
	SET @Query = @Query + N' SELECT  t2.* '
	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' 	( '
	SET @Query = @Query + N' 		SELECT AppRowId,' + @cols + ' '
	SET @Query = @Query + N' 		FROM	(  '
	SET @Query = @Query + N' 					SELECT d.AppRowId, d.CellValue,c.Name AS ColumnName '
	SET @Query = @Query + N' 					FROM [dbo].[AppCellValueINT] d '
	SET @Query = @Query + N' 					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId ' 
	SET @Query = @Query + N' WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')'
	SET @Query = @Query + N' 				) x '
	SET @Query = @Query + N' 				pivot '  
	SET @Query = @Query + N' 				( '
	SET @Query = @Query + N' 					MAX(CellValue) '
	SET @Query = @Query + N' 					FOR ColumnName in (' + @cols + ' ) '
	SET @Query = @Query + N' 				) p '
	SET @Query = @Query + N' 	) t2 '
	SET @Query = @Query + N' 	LEFT JOIN @DataTable AS t1 ON t2.AppRowId = t1.AppRowId '
	SET @Query = @Query + N' WHERE t1.AppRowId IS NULL '

	--*************************END INT table **********************************************
	--********************************************************************************************

	
	--*************************BEGIN DateTime table **********************************************
	--********************************************************************************************

	SELECT @cols = STUFF((SELECT ',' + QUOTENAME(Name) 
						FROM [dbo].[AppColumn] WHERE AppTableId = @TableId AND ([Year] IS NULL OR [Year] = @Year) AND (ClientId IS NULL OR ClientId = @ClientId) and DataTypeId = 4
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'')
	
	PRINT @cols

	DELETE FROM @ColumnTable

	SET @Counter = 0
	INSERT INTO @ColumnTable
	(StringColumn)
	SELECT splitdata FROM [dbo].[fnSplitString](@cols, ',')


	SET @Query = @Query + N' UPDATE t2'

	DECLARE rowCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT Id, StringColumn FROM @ColumnTable
	OPEN rowCursor
	FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @Counter = 0
			BEGIN
				SET @Query = @Query + ' SET t2.' + @ColumnName + ' = t1.' + @ColumnName
			END

		ELSE
			BEGIN
				SET @Query = @Query + ', t2.' + @ColumnName + ' = t1.' + @ColumnName
			END
		SET @Counter = 1
		FETCH NEXT FROM rowCursor INTO @DataTypeId, @ColumnName
	END

	CLOSE rowCursor
	DEALLOCATE rowCursor


	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' (SELECT AppRowId,' + @cols + ' FROM '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' SELECT d.AppRowId, d.CellValue,c.Name as ColumnName  FROM [dbo].[AppCellValueDateTime] d'
	SET @Query = @Query + N' INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId '
	SET @Query = @Query + N' WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')'
	SET @Query = @Query + N' ) x'
	SET @Query = @Query + N' pivot '
	SET @Query = @Query + N' ('
	SET @Query = @Query + N' MAX(CellValue)'
	SET @Query = @Query + N' FOR ColumnName in (' + @cols + ' )'
	SET @Query = @Query + N' ) p) t1 INNER JOIN @DataTable as t2 ON t1.AppRowId = t2.AppRowId  '

	SET @Query = @Query + N' INSERT INTO @DataTable ' 
	SET @Query = @Query + N' (AppRowId, ' + @cols + ' )'
	SET @Query = @Query + N' SELECT  t2.* '
	SET @Query = @Query + N' FROM '
	SET @Query = @Query + N' 	( '
	SET @Query = @Query + N' 		SELECT AppRowId,' + @cols + ' '
	SET @Query = @Query + N' 		FROM	(  '
	SET @Query = @Query + N' 					SELECT d.AppRowId, d.CellValue,c.Name AS ColumnName '
	SET @Query = @Query + N' 					FROM [dbo].[AppCellValueDateTime] d '
	SET @Query = @Query + N' 					INNER JOIN [dbo].[AppColumn] c ON d.AppColumnId = c.AppColumnId ' 
	SET @Query = @Query + N' WHERE c.AppTableId = ' + CONVERT(VARCHAR(15), @TableId) + ' AND ([Year] IS NULL OR [Year] = ' + CONVERT(VARCHAR(15), @Year) + ') AND (ClientId IS NULL OR ClientId = ' + CONVERT(VARCHAR(15), @ClientId) + ')'
	SET @Query = @Query + N' 				) x '
	SET @Query = @Query + N' 				pivot '  
	SET @Query = @Query + N' 				( '
	SET @Query = @Query + N' 					MAX(CellValue) '
	SET @Query = @Query + N' 					FOR ColumnName in (' + @cols + ' ) '
	SET @Query = @Query + N' 				) p '
	SET @Query = @Query + N' 	) t2 '
	SET @Query = @Query + N' 	LEFT JOIN @DataTable AS t1 ON t2.AppRowId = t1.AppRowId '
	SET @Query = @Query + N' WHERE t1.AppRowId IS NULL '

	--*************************END DateTime table **********************************************
	--********************************************************************************************

	SET @Query = @Query + 'SELECT * FROM @DataTable'			

	PRINT @Query
	EXEC (@Query)
    
END