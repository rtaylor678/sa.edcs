﻿CREATE TABLE [dbo].[AppLookup] (
    [AppLookupId]  INT           IDENTITY (1, 1) NOT NULL,
    [LookupTypeId] INT           NOT NULL,
    [Name]         VARCHAR (50)  NOT NULL,
    [DisplayName]  VARCHAR (250) NULL,
    [Value]        INT           NOT NULL,
    CONSTRAINT [PK_Lookup] PRIMARY KEY CLUSTERED ([AppLookupId] ASC),
    CONSTRAINT [FK_Lookup_LookupType] FOREIGN KEY ([LookupTypeId]) REFERENCES [dbo].[AppLookupType] ([AppLookupTypeId])
);

