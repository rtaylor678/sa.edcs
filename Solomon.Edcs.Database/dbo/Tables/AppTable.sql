﻿CREATE TABLE [dbo].[AppTable] (
    [AppTableId]      INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50)  NOT NULL,
    [DisplayName]     VARCHAR (250) NULL,
    [Study]           INT           NOT NULL,
    [Year]            INT           NULL,
    [Client]          INT           NULL,
    [Status]          INT           NULL,
    [IsFixedNoOfRows] BIT           NULL,
    CONSTRAINT [PK_AppTableId] PRIMARY KEY CLUSTERED ([AppTableId] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AppTable_Name_Study]
    ON [dbo].[AppTable]([Name] ASC, [Study] ASC);

