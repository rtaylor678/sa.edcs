﻿CREATE TABLE [dbo].[AppCellValueDecimal] (
    [AppCellValueDecimalId] INT             IDENTITY (1, 1) NOT NULL,
    [AppDataSetId]          INT             NOT NULL,
    [AppRowId]              INT             NOT NULL,
    [AppColumnId]           INT             NOT NULL,
    [CellValue]             DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_AppCellValueDecimal] PRIMARY KEY CLUSTERED ([AppCellValueDecimalId] ASC),
    CONSTRAINT [FK_AppCellValueDecimal_AppColumn] FOREIGN KEY ([AppColumnId]) REFERENCES [dbo].[AppColumn] ([AppColumnId]),
    CONSTRAINT [FK_AppCellValueDecimal_DataSet] FOREIGN KEY ([AppDataSetId]) REFERENCES [dbo].[AppDataSet] ([AppDataSetId])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20160427-140512]
    ON [dbo].[AppCellValueDecimal]([AppDataSetId] ASC, [AppRowId] ASC, [AppColumnId] ASC);

