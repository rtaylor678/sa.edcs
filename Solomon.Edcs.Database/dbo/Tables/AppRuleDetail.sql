﻿CREATE TABLE [dbo].[AppRuleDetail] (
    [AppRuleDetailId]               INT           IDENTITY (1, 1) NOT NULL,
    [AppRuleId]                     INT           NOT NULL,
    [ValidationTypeId]              INT           NOT NULL,
    [ConditionWithPreviousRuleStep] INT           NULL,
    [OrderId]                       INT           NULL,
    [ComparisonOperatorTypeId]      INT           NULL,
    [ComparisonValue]               VARCHAR (MAX) NULL,
    [ComparisonValueDataTypeId]     INT           NULL,
    [ComparisonColumnId]            INT           NULL,
    [ComparisonRowId]               INT           NULL,
    [ComparisonPercent]             INT           NULL,
    [YearId]                        INT           NULL,
    [ClientId]                      INT           NULL,
    [ComparisonDatasetId]           INT           NULL,
    [CustomScript]                  VARCHAR (MAX) NULL,
    [ValidationMessage]             VARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppRuleDetail] PRIMARY KEY CLUSTERED ([AppRuleDetailId] ASC),
    CONSTRAINT [FK_AppRuleDetail_AppRule] FOREIGN KEY ([AppRuleId]) REFERENCES [dbo].[AppRule] ([AppRuleId])
);

