﻿CREATE TABLE [dbo].[AppLookupType] (
    [AppLookupTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50)  NULL,
    [DisplayName]     VARCHAR (250) NULL,
    CONSTRAINT [PK_LookupType] PRIMARY KEY CLUSTERED ([AppLookupTypeId] ASC)
);

