﻿CREATE TABLE [dbo].[AppCellValue] (
    [AppCellValueId] INT             IDENTITY (1, 1) NOT NULL,
    [AppRowId]       INT             NOT NULL,
    [AppColumnId]    INT             NOT NULL,
    [ValueInt]       INT             NULL,
    [ValueText]      VARCHAR (MAX)   NULL,
    [ValueDecimal]   DECIMAL (18, 4) NULL,
    [ValueDateTime]  DATETIME        NULL,
    CONSTRAINT [PK_AppCellValue] PRIMARY KEY CLUSTERED ([AppCellValueId] ASC)
);

