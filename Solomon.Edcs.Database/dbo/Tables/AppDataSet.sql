﻿CREATE TABLE [dbo].[AppDataSet] (
    [AppDataSetId] INT           IDENTITY (1, 1) NOT NULL,
    [Study]        INT           NOT NULL,
    [Year]         INT           NOT NULL,
    [Client]       INT           NOT NULL,
    [Submission]   INT           NULL,
    [Criteria]     VARCHAR (500) NULL,
    CONSTRAINT [PK_DataSet] PRIMARY KEY CLUSTERED ([AppDataSetId] ASC)
);

