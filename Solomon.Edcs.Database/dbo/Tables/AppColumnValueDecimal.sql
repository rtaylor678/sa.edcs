﻿CREATE TABLE [dbo].[AppColumnValueDecimal] (
    [AppColumnValueDecimalId] INT             IDENTITY (1, 1) NOT NULL,
    [ColumnId]                INT             NOT NULL,
    [ColumnValue]             DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_AppColumnValueDecimal] PRIMARY KEY CLUSTERED ([AppColumnValueDecimalId] ASC)
);

