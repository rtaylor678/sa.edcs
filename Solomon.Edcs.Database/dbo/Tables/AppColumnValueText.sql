﻿CREATE TABLE [dbo].[AppColumnValueText] (
    [AppColumnValueTextId] INT           IDENTITY (1, 1) NOT NULL,
    [ColumnId]             INT           NOT NULL,
    [ColumnValue]          VARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppColumnValueText] PRIMARY KEY CLUSTERED ([AppColumnValueTextId] ASC)
);

