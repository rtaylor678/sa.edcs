﻿CREATE TABLE [dbo].[AppRuleAssociation] (
    [AppRuleAssociationId] INT           IDENTITY (1, 1) NOT NULL,
    [AppRuleId]            INT           NOT NULL,
    [AppColumnId]          INT           NOT NULL,
    [AppRowId]             INT           NULL,
    [YearId]               INT           NULL,
    [ClientId]             INT           NULL,
    [Message]              VARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppRuleAssociation] PRIMARY KEY CLUSTERED ([AppRuleAssociationId] ASC),
    CONSTRAINT [FK_AppRuleAssociation_AppColumn] FOREIGN KEY ([AppColumnId]) REFERENCES [dbo].[AppColumn] ([AppColumnId]),
    CONSTRAINT [FK_AppRuleAssociation_AppRule] FOREIGN KEY ([AppRuleId]) REFERENCES [dbo].[AppRule] ([AppRuleId])
);

