﻿CREATE TABLE [dbo].[AppColumn] (
    [AppColumnId]   INT           IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (50)  NOT NULL,
    [DisplayName]   VARCHAR (250) NULL,
    [AppTableId]    INT           NOT NULL,
    [ColumnOrder]   INT           NULL,
    [DataTypeId]    INT           NOT NULL,
    [Year]          INT           NULL,
    [ClientId]      INT           NULL,
    [IsReadOnly]    BIT           NULL,
    [IsGroupColumn] BIT           NULL,
    [Status]        INT           NULL,
    [SourceId]      INT           NULL,
    CONSTRAINT [PK_AppColumnId] PRIMARY KEY CLUSTERED ([AppColumnId] ASC)
);



