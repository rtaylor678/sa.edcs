﻿CREATE TABLE [dbo].[AppCellValueText] (
    [AppCellValueTextId] INT           IDENTITY (1, 1) NOT NULL,
    [AppDataSetId]       INT           NOT NULL,
    [AppRowId]           INT           NOT NULL,
    [AppColumnId]        INT           NOT NULL,
    [CellValue]          VARCHAR (MAX) NULL,
    CONSTRAINT [PK_AppCellValueText] PRIMARY KEY CLUSTERED ([AppCellValueTextId] ASC),
    CONSTRAINT [FK_AppCellValueText_AppColumn] FOREIGN KEY ([AppColumnId]) REFERENCES [dbo].[AppColumn] ([AppColumnId]),
    CONSTRAINT [FK_AppCellValueText_DataSet] FOREIGN KEY ([AppDataSetId]) REFERENCES [dbo].[AppDataSet] ([AppDataSetId])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20160427-141158]
    ON [dbo].[AppCellValueText]([AppDataSetId] ASC, [AppRowId] ASC, [AppColumnId] ASC);

