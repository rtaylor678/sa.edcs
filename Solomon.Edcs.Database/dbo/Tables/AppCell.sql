﻿CREATE TABLE [dbo].[AppCell] (
    [AppCellId]   INT          IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (50) NOT NULL,
    [AppColumnId] INT          NOT NULL,
    [AppRowId]    INT          NOT NULL,
    CONSTRAINT [PK_AppCell] PRIMARY KEY CLUSTERED ([AppCellId] ASC)
);

