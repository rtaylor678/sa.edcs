﻿CREATE TABLE [dbo].[AppRule] (
    [AppRuleId]   INT           IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (250) NOT NULL,
    [DisplayName] VARCHAR (500) NULL,
    [IsActive]    BIT           CONSTRAINT [DF_AppRule_IsActive] DEFAULT ((1)) NOT NULL,
    [CreateDate]  DATETIME      CONSTRAINT [DF_AppRule_CreateDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT           NOT NULL,
    [UpdateDate]  DATETIME      CONSTRAINT [DF_AppRule_UpdateDate] DEFAULT (getdate()) NOT NULL,
    [UpdatedBy]   INT           NOT NULL,
    CONSTRAINT [PK_AppRule] PRIMARY KEY CLUSTERED ([AppRuleId] ASC)
);

