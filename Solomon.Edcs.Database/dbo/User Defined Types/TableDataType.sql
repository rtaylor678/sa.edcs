﻿CREATE TYPE [dbo].[TableDataType] AS TABLE (
    [Id]                INT             NULL,
    [AppDataSetId]      INT             NULL,
    [AppRowId]          INT             NULL,
    [AppColumnId]       INT             NULL,
    [CellValueText]     VARCHAR (MAX)   NULL,
    [CellValueInt]      INT             NULL,
    [CellValueDecimal]  DECIMAL (18, 4) NULL,
    [CellValueDateTime] DATETIME        NULL);

