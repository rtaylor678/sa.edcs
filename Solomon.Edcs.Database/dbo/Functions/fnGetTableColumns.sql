﻿-- =============================================
-- Author:		Omar Azad
-- Create date: 04/14/2016
-- DescriptiON:	Returns Table columns by TableId and optional criteria
-- =============================================
CREATE FUNCTION [dbo].[fnGetTableColumns] 
(
	@TableId INT,
	@DataTypeId INT,
	@Year INT = 0,
	@ClientId INT = 0	
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Query VARCHAR(MAX)
	
	SET @Query = STUFF((SELECT ',' + QUOTENAME(Name) 
						FROM [dbo].[AppColumn] WHERE AppTableId = @TableId AND ([Year] IS NULL OR [Year] = @Year) AND (ClientId IS NULL OR ClientId = @ClientId) and DataTypeId = @DataTypeId
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'')
	
	
	RETURN @Query

END