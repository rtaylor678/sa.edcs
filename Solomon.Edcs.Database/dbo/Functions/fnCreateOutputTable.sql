﻿-- Batch submitted through debugger: SQLQuery3.sql|7|0|C:\Users\oazad.DC1\AppData\Local\Temp\~vsACEA.sql
-- =============================================
-- Author:		Omar Azad
-- Create date: 04/14/2016
-- DescriptiON:	Returns Table declare script to create table by TableId and optional criteria
-- =============================================
CREATE FUNCTION [dbo].[fnCreateOutputTable] 
(
	@TableId INT,
	@Year INT = 0,
	@ClientId INT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Query VARCHAR(MAX)
	SET @Query = N' DECLARE @DataTable TABLE(AppRowId INT '
	
	DECLARE @ColumnName VARCHAR(250), @DataTypeId INT

	DECLARE myCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT c.Name, c.DataTypeId FROM [dbo].[AppColumn] c WHERE c.AppTableId = @TableId AND ([Year] IS NULL OR [Year] = @Year) AND (ClientId IS NULL OR ClientId = @ClientId)
	OPEN myCursor
	FETCH NEXT FROM myCursor INTO @ColumnName, @DataTypeId

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		IF @DataTypeId = 1
		BEGIN
			SET @Query = @Query + ', ' + @ColumnName + ' VARCHAR(MAX)'
		END

		IF @DataTypeId = 2
		BEGIN
			SET @Query = @Query + ', ' + @ColumnName + ' DECIMAL(18, 4)'
		END

		IF @DataTypeId = 3
		BEGIN
			SET @Query = @Query + ', ' + @ColumnName + ' INT'
		END

		IF @DataTypeId = 4
		BEGIN
			SET @Query = @Query + ', ' + @ColumnName + ' DATETIME'
		END

		FETCH NEXT FROM myCursor INTO @ColumnName, @DataTypeId
	END

	CLOSE myCursor
	DEALLOCATE myCursor

	SET @Query = @Query + N')'
	
	
	RETURN @Query

END