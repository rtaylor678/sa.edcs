﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Model
{
    public class TableData
    {
        public IList<AppCellValueDateTime> AppCellValueDateTimeList { get; set; }
        public IList<AppCellValueDecimal> AppCellValueDecimalList { get; set; }
        public IList<AppCellValueText> AppCellValueTextList { get; set; }
        public IList<AppCellValueInt> AppCellValueIntList { get; set; }

        public TableData()
        {
            this.AppCellValueDateTimeList = new List<AppCellValueDateTime>();
            this.AppCellValueDecimalList = new List<AppCellValueDecimal>();
            this.AppCellValueIntList = new List<AppCellValueInt>();
            this.AppCellValueTextList = new List<AppCellValueText>();
        }
    }
}
