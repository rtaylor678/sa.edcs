﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Edcs.Model
{
    public class TableCellValue
    {
        public int AppRowId { get; set; }

        public int AppColumnId { get; set; }

        public string CellValue { get; set; }
    }
}
