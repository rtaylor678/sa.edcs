﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.Edcs.Model;
using System.Data.Entity.Core.Objects;
using System.Dynamic;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.SqlClient;


namespace Solomon.Edcs.Components
{
    public partial class AppTableManager
    {
        public IList<AppTable> GetTableList(int study, int? year = 0, int? clientId = 0)
        {

            using (var db = new EdcsEntities())
            {

                return db.GetTableList(study, year, clientId)
                    .Select(x => new AppTable
                        {
                            AppTableId = x.AppTableId,
                            Client = x.Client,
                            DisplayName = x.DisplayName,
                            Name = x.Name,
                            Status = x.Status,
                            Study = x.Study,
                            Year = x.Year
                        }).ToList();
            }
        }

        public IList<GetTableDataCellValue_Result> GetTableData(int tableId, int? year = 0, int? clientId = 0, int? dataSetId = 0)
        {
            using (var db = new EdcsEntities())
            {
                return db.GetTableDataCellValue(tableId, year, clientId, dataSetId).ToList();
            }
        }

        public IList<dynamic> GetDynamicTableData(int tableId, int? year = 0, int? clientId = 0, int dataSetId = 0)
        {
            var columns = new AppColumnManager(base.Uow).GetAll(x => x.AppTableId == tableId && (x.Year == null || x.Year == year) && (x.ClientId == null || x.ClientId == clientId)).ToList();

            var data = GetTableData(tableId, year, clientId, dataSetId);
            IList<dynamic> dataTable = new List<dynamic>();

            var dataSets = data.Select(x => new { x.AppDataSetId.Value }).Distinct().ToList();

            foreach (var datasetId in dataSets)
            {
                var rows = data.Where(x => x.AppDataSetId.Value == datasetId.Value).Select(x => new { x.AppDataSetId, x.RowId }).Distinct().ToList();
                foreach (var row in rows)
                {
                    var dataRow = new ExpandoObject() as IDictionary<string, object>;
                    dataRow.Add("AppRowId", row.RowId.Value);

                    for (int i = 0; i < columns.Count; i++)
                    {
                        var result = data.Where(x => x.RowId == row.RowId & x.AppDataSetId == row.AppDataSetId && x.ColumnName == columns[i].Name).FirstOrDefault();
                        if (result != null)
                        {
                            dataRow[columns[i].Name] = result.CellValue;
                        }
                        else
                        {
                            dataRow[columns[i].Name] = "";
                        }
                    }
                    dataTable.Add(dataRow);
                }
            }

            return dataTable;
        }

        public static bool SaveTableData(int dataSet, List<TableCellValue> data)
        {
            using (var db = new EdcsEntities())
            {
                
                var context = (db as IObjectContextAdapter).ObjectContext;

                var dt = new DataTable();
                dt.Columns.Add("AppRowId");
                dt.Columns.Add("AppColumnId");
                dt.Columns.Add("CellValue");

                foreach (var item in data)
                {
                    dt.Rows.Add
                        (
                            item.AppRowId,
                            item.AppColumnId,
                            item.CellValue
                        );
                }

                var dataSetId = new SqlParameter("DataSetId", SqlDbType.Int);
                dataSetId.Value = dataSet;

                var cellValue = new SqlParameter("CellValue", SqlDbType.Structured);
                cellValue.Value = dt;
                cellValue.TypeName = "dbo.CellValueType";

                context.ExecuteStoreCommand("Exec [dbo].[SaveCellValue] @DataSetId, @CellValue", dataSetId, cellValue);
            }

            return true;
        }
    }
}
