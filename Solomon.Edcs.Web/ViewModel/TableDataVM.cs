﻿using Solomon.Edcs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solomon.Edcs.Web.ViewModel
{
    public class TableDataVM
    {
        public IList<AppColumn> Columns { get; set; }
        public IList<dynamic> DataSource { get; set; }
        public AppTable TableName { get; set; }
        public int WorkflowId { get; set; }
        public int WorkflowStepId { get; set; }
    }
}