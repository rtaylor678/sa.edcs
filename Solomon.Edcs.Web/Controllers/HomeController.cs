﻿using Newtonsoft.Json;
using Solomon.Edcs.Model;
using Solomon.Edcs.Services;
using Solomon.Edcs.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Solomon.Edcs.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "About";

            return View();
        }

        public JsonResult GetTablesByStudy(string study)
        {
            if (!string.IsNullOrWhiteSpace(study))
            {
                var service = new AppTableService();

                return Json(service.GetTableList(int.Parse(study)).ToList(), JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        [HttpPost]
        public bool SubmitTableData(int dataSet, string input)
        {

            var tableData = JsonConvert.DeserializeObject<List<TableCellValue>>(input);

            return new AppTableService().SaveCellValue(dataSet, tableData);

        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact";

            return View();
        }

        public ActionResult GetWorkflow()
        {
            int workFlowId = 0;                
            int.TryParse(Request.QueryString["WorkflowId"], out workFlowId);
            
            int workflowStepId = 0;
            int.TryParse(Request.QueryString["WorkflowStepId"], out workflowStepId);
            
            string buttonType = Request.QueryString["ButtonType"].ToString();

            if (workFlowId > 0)
            {
                var workflowSteps = new WorkflowStepService().GetAll(x => x.WorkflowId == workFlowId);
                var currentWorkflowStep = workflowSteps.Where(x => x.WorkflowStepId == workflowStepId).SingleOrDefault();

                if (currentWorkflowStep != null)
                {
                    var actionableStepId = 1;
                    if (buttonType == "Next")
                    {   
                        if (currentWorkflowStep.NextStepId.HasValue)
                        {
                            actionableStepId = currentWorkflowStep.NextStepId.Value;
                        }
                    }

                    if (buttonType == "Previous")
                    {
                        if (currentWorkflowStep.PreviousStepId.HasValue)
                        {
                            actionableStepId = currentWorkflowStep.PreviousStepId.Value;
                        }
                    }

                    var actionableStep = workflowSteps.Where(x => x.WorkflowStepId == actionableStepId).SingleOrDefault();

                    if (actionableStep != null)
                    {
                        return GetWorkflowView(actionableStep);
                    }
                }
            }
            return View();
        }

        public ActionResult TableData()
        {
            ViewBag.Message = "Table Data";

            var id = Request.QueryString["tableId"];

            if (!string.IsNullOrWhiteSpace(id))
            {
                int tableId = int.Parse(id);
                var model = new TableDataVM();

                model.WorkflowId = 1;
                model.WorkflowStepId = 1;
                model.TableName = new AppTableService().GetById(tableId);
                model.Columns = new AppColumnService().GetColumnList(tableId);
                model.Columns.Add(new AppColumn { Name = "AppRowId", AppColumnId = 0, DataTypeId = 1 });
                var data = new AppTableService().GetDynamicTableData(tableId, dataSetId: 1);
                model.DataSource = data;
                return View(model);
            }

            return View();
        }

        private ActionResult GetWorkflowView(WorkflowStep wfs)
        {
            var model = new TableDataVM();
            
            //TODO: Dataset Id has to come from User Info 
            var datasetId = 1;
            
            model.WorkflowId = wfs.WorkflowId;
            model.WorkflowStepId = wfs.WorkflowStepId;

            if (wfs.SourceTableId.HasValue)
            {
                model.TableName = new AppTableService().GetById(wfs.SourceTableId.Value);
                model.Columns = new AppColumnService().GetColumnList(wfs.SourceTableId.Value);
                model.Columns.Add(new AppColumn { Name = "AppRowId", AppColumnId = 0, DataTypeId = 1 });
                var data = new AppTableService().GetDynamicTableData(wfs.SourceTableId.Value, dataSetId: datasetId);

                model.DataSource = data;
            }
            
            //Do a factory
            if (wfs.PageType.HasValue && wfs.PageType.Value == 1)
            {
                return View("TableData", model);
            }
            else if (wfs.PageType.HasValue && wfs.PageType.Value == 2)
            {
                return View("SingleColumnForm", model);
            }
            else if (wfs.PageType.HasValue && wfs.PageType.Value == 3)
            {
                return View("DoubleColumnForm", model);
            }


            return View();
        }
    }
}
